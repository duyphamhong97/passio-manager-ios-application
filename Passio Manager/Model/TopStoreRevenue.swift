//
//  TopStoreRevenue.swift
//  Passio Manager
//
//  Created by UniMob on 9/6/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper

class TopStoreRevenue : Mappable {
    
    var storeName:String =  ""
    var totalStoreOrder:Int = 0
    var totalStoreRevenue:Int = 0
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        storeName                                      <- map["store_name"]
        totalStoreOrder                                <- map["total_store_order"]
        totalStoreRevenue                              <- map["total_store_revenue"]
    }
}
