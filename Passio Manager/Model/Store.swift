//
//  Store.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 10/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

class Store : Object, Mappable {
    @objc dynamic var StorePI = "StorePI"
    @objc dynamic var storeID : Int = 0
    @objc dynamic var storeName : String = ""
    @objc dynamic var district : String = ""
    @objc dynamic var province : String = ""
    @objc dynamic var address : String = ""

    required init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map : Map){
        storeID                                      <- map["store_id"]
        storeName                                    <- map["store_name"]
        district                                    <- map["district"]
        province                                    <- map["province"]
        address                                    <- map["address"]
    }
    
    override static func primaryKey() -> String? {
        return "StorePI"
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm,schema:schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
