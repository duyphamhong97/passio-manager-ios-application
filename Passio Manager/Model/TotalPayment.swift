//
//  TotalPayment.swift
//  Passio Manager
//
//  Created by UniMob on 9/6/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper

class TotalPayment : Mappable {
    
    var totalPayment:Int = 0
    var totalPaymentCash:Int = 0
    var totalPaymentOrderCard:Int = 0
    var totalPaymentCard:Int = 0
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        totalPayment                                         <- map["total_payment"]
        totalPaymentCash                                     <- map["total_payment_cash"]
        totalPaymentOrderCard                                <- map["total_payment_ordercard"]
        totalPaymentCard                                     <- map["total_payment_card"]
    }
}
