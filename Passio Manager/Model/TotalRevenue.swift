//
//  TotalRevenue.swift
//  Passio Manager
//
//  Created by UniMob on 9/6/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper

class TotalRevenue : Mappable {
    
    var total:Int = 0
    var totalReal:Int = 0
    var discount:Int = 0
    var final:Int = 0
    var orderCard:Int = 0
    var atStore : Int = 0
    var takeAway : Int = 0
    var delivery : Int = 0
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        total                            <- map["total"]
        totalReal                        <- map["total_real"]
        discount                         <- map["discount"]
        final                            <- map["final"]
        orderCard                        <- map["total_order_card"]
        atStore                          <- map["total_at_store"]
        takeAway                         <- map["total_take_away"]
        delivery                         <- map["total_delivery"]
    }
}
