//
//  Login.swift
//  Passio Manager
//
//  Created by UniMob on 9/10/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

class User : Object, Mappable {
    
    @objc dynamic var priKeyForPI = "PIUser"
    @objc dynamic var userId:String = ""
    @objc dynamic var userName:String = ""
    @objc dynamic var fullName:String = ""
    @objc dynamic var accessToken:String = ""
    
    required init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        userId                              <- map["user_id"]
        userName                            <- map["user_name"]
        fullName                            <- map["full_name"]
        accessToken                         <- map["access_token"]
    }
    
    override static func primaryKey() -> String? {
        return "priKeyForPI"
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm,schema:schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
}
