//
//  Report.swift
//  Passio Manager
//
//  Created by UniMob on 9/6/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper


class Report : Mappable {
    
    var dataChart = DataChart()
    var totalRevenue = TotalRevenue()
    var totalQuantity = TotalQuantity()
    var totalPayment = TotalPayment()
    var totalPaymentTransaction = TotalPaymentTransaction()
    var topStoreRevenue = TopStoreRevenue()
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        dataChart                               <- map["data_chart"]
        totalRevenue                            <- map["total_revenue"]
        totalQuantity                           <- map["total_quantity"]
        totalPayment                            <- map["total_payment"]
        totalPaymentTransaction                 <- map["total_payment_transaction"]
        topStoreRevenue                         <- map["top_store_revenue"]
    }
}
