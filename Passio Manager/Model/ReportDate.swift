//
//  ReportHour.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 14/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper


class ReportDate : Mappable {
    
    var dataChart = DataChartDate()
    var totalRevenue = TotalRevenue()
    var totalQuantity = TotalQuantity()
    var totalPayment = TotalPayment()
    var totalPaymentTransaction = TotalPaymentTransaction()
    var topStoreRevenue = TopStoreRevenue()
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        dataChart                               <- map["data_chart"]
        totalRevenue                            <- map["total_revenue"]
        totalQuantity                           <- map["total_quantity"]
        totalPayment                            <- map["total_payment"]
        totalPaymentTransaction                 <- map["total_payment_transaction"]
        topStoreRevenue                         <- map["top_store_revenue"]
    }
}
