//
//  TotalPaymentTransaction.swift
//  Passio Manager
//
//  Created by UniMob on 9/6/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper

class TotalPaymentTransaction : Mappable {
    
    var totalTransaction:Int = 0
    var orderCardTransaction:Int = 0
    var cardTransaction:Int = 0
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        totalTransaction                                         <- map["total_transaction"]
        orderCardTransaction                                     <- map["order_card_transaction"]
        cardTransaction                                          <- map["card_transaction"]
    }
}
