//
//  DataChartDate.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 14/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper

class DataChartDate : Mappable {
    
    var time:[String] = []
    var quantity:[Int] = []
    var price:[Int] = []
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        time                    <- map["date_name"]
        quantity                <- map["quantity"]
        price                   <- map["price"]
    }
}
