//
//  Product.swift
//  Passio Manager
//
//  Created by UniMob on 9/10/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper

class Product : Mappable {
    
    var img:String = ""
    var name:String = ""
    var ratio:Int = 0
    var quantity:Int = 0
    var totalAmount:Int = 0
    var discount:Int = 0
    var finalAmount:Int = 0
    
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    required init() {
    }
    
    func mapping(map: Map) {
        name                        <- map["name"]
        ratio                       <- map["ratio"]
        quantity                    <- map["quantity"]
        totalAmount                 <- map["total_amount"]
        discount                    <- map["discout"]
        finalAmount                 <- map["final_amount"]
        img                         <- map["pic_url"]
    }
}
