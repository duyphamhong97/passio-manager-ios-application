//
//  TotalQuantity.swift
//  Passio Manager
//
//  Created by UniMob on 9/6/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper

class TotalQuantity : Mappable {
    
    var totalOrder:Int = 0
    var atStore:Int = 0
    var takeAway:Int = 0
    var delivery:Int = 0
    var orderCard:Int = 0
    
    required init() {
        
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        totalOrder                              <- map["total_order"]
        atStore                                 <- map["at_store"]
        takeAway                                <- map["take_away"]
        delivery                                <- map["delivery"]
        orderCard                               <- map["order_card"]
    }
}
