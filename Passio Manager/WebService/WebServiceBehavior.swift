//
//  WebServiceBehavior.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 7/3/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import ReactiveSwift


protocol HomeBehavior {
//    func getBlogPosts() -> SignalProducer<BlogPostResponse, NSError>
}
protocol ReportBehavior {
    func getHourReport(accessToken:String, dateStart: String, dateEnd:String, StoreID:Int) -> SignalProducer<Report, NSError>
}
protocol LoginBehavior {
    func login(username:String, password:String) -> SignalProducer<User, NSError>
}
protocol ProductReportBehavior {
    func getProductReport(startTime:String, endTime: String, selectedStoreId: Int, accessToken: String, skip: Int, take: Int, OrderByEnum: Int) -> SignalProducer<[Product], NSError>
}
protocol GetUserStoreBehavior {
    func getUserStore(accessToken : String) -> SignalProducer<[Store], NSError>
}
protocol GetDateReportBehavior {
    func getDateReport(accessToken:String, dateStart: String, dateEnd:String, StoreID:Int) -> SignalProducer<ReportDate, NSError>
}
protocol GetTopStoreReportBehavior {
    func getTopStore(startTime:String, endTime: String, selectedStoreId: Int, accessToken: String, skip: Int, take: Int, orderByDes: Bool) -> SignalProducer<[TopStoreRevenue], NSError>
}
