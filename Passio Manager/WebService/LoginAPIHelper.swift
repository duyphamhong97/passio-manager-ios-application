//
//  LoginAPI.swift
//  Passio Manager
//
//  Created by UniMob on 9/10/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper
import ReactiveSwift
import Alamofire



class LoginAPIHelper: BaseAPIHelper,LoginBehavior {
    func login(username: String, password: String) -> SignalProducer<User, NSError> {
        return SignalProducer<User,NSError> {
            (sink,_) in
            
            let requestUrl = AppURL.login
            let parameters = ["brandId":BrandId, "username":username, "password": password] as Dictionary<String,String>
            self.alamofireManager.request(requestUrl, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any] {
                    
                    if let status = value["status"] as? [String:Any], let success = status["success"] as? Bool,
                        success == true {
                        
                        if let data = value["data"] as? [String : Any] {
                            if let responseData = data["data"] as? [String : Any] {
                                let user = Mapper<User>().map(JSON: responseData)
                                sink.send(value: user!)
                                sink.sendCompleted()
                            } else {
                                let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                                sink.send(error: error)
                            }
                        } else {
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    }else {
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                    
                } else {
                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
                
            })
            
            
        }
    }
    
    
}
