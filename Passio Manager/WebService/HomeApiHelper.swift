//
//  HomeApiHelper.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 7/3/18.
//  Copyright © 2018 Passio. All rights reserved.
//

//import Foundation
//import Alamofire
//import ObjectMapper
//import ReactiveSwift
//
//
//class HomeApiHelper : BaseAPIHelper, HomeBehavior {
//
//    func getBlogPosts() -> SignalProducer<BlogPostResponse, NSError> {
//        return SignalProducer<BlogPostResponse, NSError> {
//            (sink,_) in
//
//            let requestUrl = AppURL.GetBlogPost
//
//            self.alamofireManager.request(requestUrl, method: .get, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
//
//                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any] {
//
//                    if let status = value["status"] as? [String:Any], let success = status["success"] as? Bool,
//                        success == true {
//
//                        if let data = value["data"] as? [String : Any] {
//                            if let responseData = data["data"] as? [String : Any] {
//                                let blogPosts = Mapper<BlogPostResponse>().map(JSON: responseData)
//                                sink.send(value: blogPosts!)
//                                sink.sendCompleted()
//                            } else {
//                                let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
//                                sink.send(error: error)
//                            }
//                        } else {
//                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
//                            sink.send(error: error)
//                        }
//                    }else {
//                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
//                        sink.send(error: error)
//                    }
//                    
//                } else {
//                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
//                    sink.send(error: error)
//                }
//
//            })
//
//        }
//    }
//
//}
