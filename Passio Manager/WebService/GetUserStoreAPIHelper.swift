//
//  GetUserStoreAPIHelper.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 10/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import ReactiveSwift

class GetUserStoreAPIHelper: BaseAPIHelper, GetUserStoreBehavior {
    func getUserStore(accessToken: String) -> SignalProducer<[Store], NSError> {

        return SignalProducer<[Store], NSError>{
            (sink,_) in
            
            let requestUrl = AppURL.GetUserStore
            let parameters = ["accessToken": accessToken] as Dictionary<String, String>
            
            self.alamofireManager.request(requestUrl, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: self.header).responseJSON( completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any]{
                    
                    if let status = value["status"] as? [String:Any], let success = status["success"] as? Bool,
                        success == false {
                        
                        if let responseData = value["data"] as? [String: Any]
                        {
                            if let data = responseData["data"] as? [[String : Any]] {
                                
                                let storeList = data.map({ showing in
                                    Mapper<Store>().map(JSON: showing)
                                })
                                sink.send(value: storeList as! [Store])
                                sink.sendCompleted()
                                
                            }else{
                                let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                                sink.send(error: error)
                            }
                        }else{
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    }else{
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                }else{
                    let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
                
            }
            )
        }
    }
    
}
