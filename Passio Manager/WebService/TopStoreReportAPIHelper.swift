//
//  TopStoreReportAPIHelper.swift
//  Passio Manager
//
//  Created by UniMob on 9/17/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper
import ReactiveSwift
import Alamofire

class TopStoreReportAPIHelper: BaseAPIHelper, GetTopStoreReportBehavior {
    func getTopStore(startTime: String, endTime: String, selectedStoreId: Int, accessToken: String, skip: Int, take: Int, orderByDes: Bool) -> SignalProducer<[TopStoreRevenue], NSError> {
            return SignalProducer<[TopStoreRevenue], NSError> {
                (sink,_) in
                
                let parameters = ["accessToken" : accessToken, "brandId": BrandId, "endTime": endTime, "startTime": startTime, "selectedStoreId": selectedStoreId , "skip": skip, "take": take, "orderByDes": orderByDes] as [String : Any]
                let requestUrl = AppURL.GetTopstore
                
                self.alamofireManager.request(requestUrl, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                    
                    if response.response?.statusCode == 200, let value = response.result.value as? [String : Any] {
                        
                        if let error:NSError = self.checkStatus(value: value) {
                            if (error.code != 200) {
                                sink.send(error: error)
                                return
                            }
                        }
                        
                        if let responseData = value["data"] as? [String: Any]
                        {
                            if let product = responseData["data"] as? [String : Any] {
                                if let topStore = product["top_store_revenue"] as? [[String : Any]] {
                                    let topStoreList = topStore.map({ showing in
                                        Mapper<TopStoreRevenue>().map(JSON: showing)
                                    })
                                    sink.send(value: topStoreList as! [TopStoreRevenue])
                                    sink.sendCompleted()
                                } else {
                                    let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                                    sink.send(error: error)
                                }
                                
                            } else {
                                let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                                sink.send(error: error)
                            }
                        } else {
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    } else {
                        let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                    
                })
                
            }
    }
}
