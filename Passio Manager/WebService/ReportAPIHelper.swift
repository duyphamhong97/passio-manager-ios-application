//
//  ReportAPIHelper.swift
//  Passio Manager
//
//  Created by UniMob on 9/6/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import ReactiveSwift


class ReportAPIHelper : BaseAPIHelper, ReportBehavior{
    func getHourReport(accessToken:String, dateStart: String, dateEnd:String, StoreID:Int) -> SignalProducer<Report, NSError> {
        return SignalProducer<Report, NSError> {
            (sink,_) in
            
            let requestUrl = AppURL.GetHourReport
//            print(accessToken)
            let parameters = ["brandId":BrandId, "startTime":dateStart, "endTime":dateEnd, "selectedStoreId": StoreID, "accessToken":accessToken] as? Dictionary<String, Any>
            self.alamofireManager.request(requestUrl, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any] {
                    
                    if let status = value["status"] as? [String:Any], let success = status["success"] as? Bool,
                        success == true {
                        
                        if let data = value["data"] as? [String : Any] {
                            if let responseData = data["data"] as? [String : Any] {
                                let report = Mapper<Report>().map(JSON: responseData)
                                sink.send(value: report!)
                                sink.sendCompleted()
                            } else {
                                let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                                sink.send(error: error)
                            }
                        } else {
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    }else {
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                    
                } else {
                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
                
            })
            
        }
    }
    
    
}
