//
//  ProductReport.swift
//  Passio Manager
//
//  Created by UniMob on 9/10/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import ObjectMapper
import ReactiveSwift
import Alamofire

class ProductReportAPIHelper: BaseAPIHelper, ProductReportBehavior {
    func getProductReport(startTime: String, endTime: String, selectedStoreId: Int, accessToken: String, skip: Int, take: Int, OrderByEnum: Int) -> SignalProducer<[Product], NSError> {
        return SignalProducer<[Product], NSError> {
            (sink,_) in
            
            let parameters = ["accessToken" : accessToken, "brandId": BrandId, "endTime": endTime, "startTime": startTime, "selectedStoreId": selectedStoreId , "skip": skip, "take": take, "OrderByEnum": OrderByEnum ] as [String : Any]
            let requestUrl = AppURL.getProductReport
            
            self.alamofireManager.request(requestUrl, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any] {
                    
                    if let error:NSError = self.checkStatus(value: value) {
                        if (error.code != 200) {
                            sink.send(error: error)
                            return
                        }
                    }
                    
                    if let responseData = value["data"] as? [String: Any]
                    {
                        if let product = responseData["data"] as? [String : Any] {
                            if let productData = product["product_data"] as? [[String : Any]] {
                                let productList = productData.map({ showing in
                                    Mapper<Product>().map(JSON: showing)
                                })
                                sink.send(value: productList as! [Product])
                                sink.sendCompleted()
                            } else {
                                let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                                sink.send(error: error)
                            }
                            
                        } else {
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    } else {
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                } else {
                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
                
            })
            
        }
    }
    
}
