//
//  BaseAPIHelper.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 7/3/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift

public class BaseAPIHelper {
    var header: [String : String] = [:]
    var parameter: [String : Any] = [:]
    let alamofireManager = Alamofire.SessionManager.default
    
    required public init() {
        setHeader()
        setTimeOut()
    }
    
    func setHeader() {
        
    }
    
    func setParameter(parameter: [String : Any]) {
        self.parameter = parameter
    }
    
    func setTimeOut() {
//        self.alamofireManager.session.configuration.timeoutIntervalForRequest = 15
//        self.alamofireManager.session.configuration.timeoutIntervalForResource = 15
    }
    
    func checkStatus(value:[String : Any] ) -> NSError? {
        
        if let responseStatus = value["status"] as? [String: Any] {
            let isSuccess = responseStatus["success"] as? Bool
            let status = responseStatus["status"] as? Int
            let message = responseStatus["message"] as? String
            
            if isSuccess != true || status != 200 {
                let error = NSError(domain: message ?? "Server Error", code: 0, userInfo: nil)
                return error
            }
        } else {
            let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
            return error
        }
        return NSError(domain: "Server Accepted", code: 200, userInfo: nil)
    }
    
}

