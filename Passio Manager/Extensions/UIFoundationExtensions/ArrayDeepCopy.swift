//
//  ArrayDeepCopy.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 7/18/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
//import RealmSwift
//import Realm

//Protocal that copyable class should conform
protocol Copying {
    init(original: Self)
}

//Concrete class extension
extension Copying {
    func copy() -> Self {
        return Self.init(original: self)
    }
}

//Array extension for elements conforms the Copying protocol
extension Array where Element: Copying {
    func clone() -> Array {
        var copiedArray = Array<Element>()
        for element in self {
            copiedArray.append(element.copy())
        }
        return copiedArray
    }
}

protocol DetachedableObject: AnyObject {
    func detached() -> Self
}

//extension Object: DetachedableObject {
//    
//    public func detached() -> Self {
//        
//        let detached = type(of: self).init()
//        for property in objectSchema.properties {
//            guard let value = value(forKey: property.name) else { continue }
//            
//            if property.isArray == true {
//                let detachable = value as? DetachedableObject
//                detached.setValue(detachable?.detached(), forKey: property.name)
//            } else if property.type == .object {
//                let detachable = value as? DetachedableObject
//                detached.setValue(detachable?.detached(), forKey: property.name)
//            } else {
//                detached.setValue(value, forKey: property.name)
//            }
//        }
//        return detached
//    }
//    
//}
//
//extension List: DetachedableObject {
//    func detached() -> List<Element> {
//        let result = List<Element>()
//        
//        forEach {
//            if let detachable = $0 as? DetachedableObject {
//                let detached = detachable.detached() as! Element
//                result.append(detached)
//            }else{
//                result.append($0)
//            }
//        }
//        return result
//    }
//}
//
//extension Results {
//    func toArray() -> [Element] {
//        let result = List<Element>()
//        
//        forEach { result.append($0)
//        }
//        
//        return Array(result.detached())
//        
//    }
//}
//
//extension Sequence  where Iterator.Element:Object  {
//    
//    public var detached:[Element] {
//        return self.map({ $0.detached() })
//    }
//    
//}
