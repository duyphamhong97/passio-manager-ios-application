//
//  StringExtension.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 7/13/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation


extension String {
   
    
    func firstCharacterUpperCase() -> String {
        let lowercaseString = self.lowercased()
        
        return lowercaseString.replacingCharacters(in: lowercaseString.startIndex...lowercaseString.startIndex, with: String(lowercaseString[lowercaseString.startIndex]).uppercased())
    }
    
}
