//
//  UIViewControllerExtension.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 7/1/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func setupNavigation(title:String){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = title
        self.navigationController!.navigationBar.backItem!.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: self, action: nil)
    }

    func hideKeyboardWhenTappedAround() {
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

