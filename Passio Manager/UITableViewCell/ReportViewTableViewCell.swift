//
//  ReportViewTableViewCell.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 05/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ReportViewTableViewCell: UITableViewCell {

    @IBOutlet weak var mImgfood: UIImageView!
    @IBOutlet weak var mLblName: UILabel!
    @IBOutlet weak var mLblPrice: UILabel!
    @IBOutlet weak var mLblQuantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
