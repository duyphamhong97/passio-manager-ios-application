//
//  ReportFilterTableViewCell.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ReportFilterTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imgCheckbox: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
