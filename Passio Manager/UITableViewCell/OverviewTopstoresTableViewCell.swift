//
//  OverviewTopstoresTableViewCell.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class OverviewTopstoresTableViewCell: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewCell.setRadius(radius: 10)
        btnNumber.layer.cornerRadius = btnNumber.frame.width/2
        btnNumber.clipsToBounds = true
    }

}
