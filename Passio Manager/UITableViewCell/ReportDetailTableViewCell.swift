//
//  ReportDetailTableViewCell.swift
//  Passio Manager
//
//  code: duyPH
//  Created by UniMob on 9/10/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ReportDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDrinkName: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lblNumber: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        viewCell.setRadius(radius: 10)
        lblNumber.layer.cornerRadius = lblNumber.frame.width/2
        lblNumber.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setupData(data: Product) {
//        self.lblDrinkName.text = data.name
//        self.lbl1.text = String(data.quantity)
//        self.lbl2.text = String(data.ratio)
//        self.lbl3.text = String(data.totalAmount)
//        self.lbl4.text = String(data.discount)
//        self.lbl5.text = String(data.finalAmount)
    }
    
}
