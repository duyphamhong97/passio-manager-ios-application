//
//  TestTableViewCell.swift
//  Passio Manager
//
//  Created by UniMob on 8/29/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class StoreListTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblDistrict: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
//    func setupData(data:Store, section:Int, row:Int, open:Bool, isChoose:Bool) {
//        self.lblTitle.text = data.storeName
//        if row == 0 {
//            self.lblTitle.font = UIFont(name: "Muli-SemiBold", size: 19)
//            self.lblTitle.textColor = Color.charcoal()
//        } else {
//            self.lblTitle.font = UIFont(name: "Muli-Regular", size: 16)
//            self.lblTitle.textColor = Color.dark_sage()
//        }
//        if row == 0 && section != 0 {
//            if open == true {
//                self.img.image = UIImage(named: "ic_arrow_up")
//            } else {
//                self.img.image = UIImage(named: "ic_arrow_down")
//            }
//        } else {
//            if isChoose {
//                self.img.image = UIImage(named: "ic_active")
//            } else {
//                self.img.image = UIImage(named: "ic_inactive")
//            }
//        }
//        if row > 0 {
//            self.separatorInset = UIEdgeInsets(top: 0, left: view.frame.width, bottom: 0, right: 16);
//        } else {
//            self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16);
//        }
//        self.view.setRadius(radius: 2)
//    }

    func setupData(data:Store, section:Int, isChoose:Bool) {
        self.lblTitle.text = data.storeName
        self.lblDistrict.text = data.address
        if isChoose {
            img.image = UIImage(named: "ic_check")
        }else{
            img.image = UIImage(named: "ic_normal")
        }
        self.view.setRadius(radius: 2)
        
    }

}
