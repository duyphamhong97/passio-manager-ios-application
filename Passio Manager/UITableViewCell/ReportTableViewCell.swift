//
//  ReportTableViewCell.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 05/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ReportTableViewCell: UITableViewCell {

    @IBOutlet weak var mImgCheck: UIImageView!
    @IBOutlet weak var mLblDiscount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
