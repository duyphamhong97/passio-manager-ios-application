//
//  OverviewTopstoresSortTableViewCell.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class OverviewTopstoresSortTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
