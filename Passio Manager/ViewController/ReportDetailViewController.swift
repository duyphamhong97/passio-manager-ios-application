//
//  ReportDetailViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/10/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import JGProgressHUD

class ReportDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBAction func btnSort(_ sender: Any) {
//        let sortViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewTopstoresSortViewController) as! OverviewTopstoresSortViewController
//        self.navigationController?.pushViewController(sortViewController, animated: true)
//
//        sortViewController.checkTypeSort = self.checkTypeSort
//        sortViewController.delegate = self
        
        self.present(controller, animated: true, completion: nil)

    }
    
    var data = [Product]()
    var newData = [Product]()
    var incEveryQuery:Int = 5
    var numberOfElement:Int = 5 // = incEveryQuery
    var page = 1
    var maxOfNumber:Int = 1000
    var orderBy:Int = 0 // 0: quantity, 1: total amount, 2: name 2
    
//    var checkTypeSort:Bool = true
    
    var controller : UIAlertController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        loadingDataFromServer(page, incEveryQuery)
        self.scrollView.addSubview(self.refreshControl)

        actionSheet()
    }
    
//    func getOrderBy(orderBySort: Int) {
//        orderBy = orderBySort
//        newData = []
//        incEveryQuery = 5
//        numberOfElement = 5
//        page = 1
//        maxOfNumber = 1000
//        loadingDataFromServer(page, incEveryQuery)
//    }
    
    func actionSheet() {
        controller = UIAlertController(title: "Sắp xếp theo", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let name = UIAlertAction(title: "Tên", style: UIAlertActionStyle.default) {
            (action) -> Void in
            self.orderBy = 2
            self.sortChange()
        }
        let quantity = UIAlertAction(title: "Số lượng", style: UIAlertActionStyle.default) {
            (action) -> Void in
            self.orderBy = 0
            self.sortChange()
        }
        let finalAmount = UIAlertAction(title: "Tổng tiền sau giảm giá", style: UIAlertActionStyle.default) {
            (action) -> Void in
            self.orderBy = 1
            self.sortChange()
        }
        controller.addAction(name)
        controller.addAction(quantity)
        controller.addAction(finalAmount)
        controller.view.tintColor = ConvertManager.hexStringToUIColor(hex: "a6ce39")

    }
    
    func sortChange() {
        newData = []
        incEveryQuery = 5
        numberOfElement = 5
        page = 1
        maxOfNumber = 1000
        loadingDataFromServer(page, incEveryQuery)
    }

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = ConvertManager.hexStringToUIColor(hex: "#a6ce39")
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if NetworkManager.isConnectToInternet() {
            newData = []
            incEveryQuery = 5
            page = 1
            maxOfNumber = 1000

            self.loadingDataFromServer(self.page, self.incEveryQuery)

          }
        refreshControl.endRefreshing()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ReportDetailCell") as! ReportDetailTableViewCell
        cell.lblDrinkName.text = data[indexPath.row].name
        cell.lbl1.text = String(data[indexPath.row].quantity ) + " sp"
        cell.lbl2.text = String(data[indexPath.row].ratio ) + " %"
        cell.lbl3.text = ConvertManager.formatNumberToMoney(data: data[indexPath.row].totalAmount, currencyType: Currency.VND)
        cell.lbl4.text = String(data[indexPath.row].discount ) + " đ"
        cell.lbl5.text = ConvertManager.formatNumberToMoney(data: data[indexPath.row].finalAmount, currencyType: Currency.VND)
        cell.lblNumber.text = String(indexPath.row+1)
        return cell
    }
    
    func loadingDataFromServer(_ _pageCurrent:Int, _ _pageLimit:Int) {
        
        let productReportBehavior : ProductReportBehavior = ProductReportAPIHelper()
        let userDefault:UserDefaults = UserDefaults()
        var dateBegin = ""
        var dateEnd = ""
        if ((userDefault.object(forKey: "dateBegin") != nil) && (userDefault.object(forKey: "dateEnd") != nil)){
            dateBegin = (userDefault.object(forKey: "dateBegin") as? String)!
            dateEnd = (userDefault.object(forKey: "dateEnd") as? String)!
        } else {
            let formatter = DateFormatter()
            let date = Date()
            formatter.dateFormat = "dd/MM/yyyy"
            let selectedDate = formatter.string(from: date)
            dateBegin = selectedDate
            dateEnd = selectedDate
        }
            productReportBehavior.getProductReport(startTime: dateBegin, endTime: dateEnd, selectedStoreId: (StoreManager.getStore()?.storeID)!, accessToken: (UserManager.getInfo()?.accessToken)!, skip: _pageLimit * (_pageCurrent - 1), take: _pageLimit, OrderByEnum: orderBy).on(
                failed: { error in
                    print("bbbb")
            },
                value: { product in
                    for item in product {
                        self.newData.append(item)
                    }
                    if product.isEmpty || product.count < _pageLimit {
                        self.maxOfNumber = self.newData.count
                    }
                    
                    self.data = self.newData
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
            }
                ).start()
    }
}


extension ReportDetailViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 5 {
            self.loadMore()
        }
    }
    
    func loadMore() {
        guard !(self.numberOfElement >= self.maxOfNumber) else {
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            self.numberOfElement = min(self.numberOfElement + self.incEveryQuery, self.maxOfNumber)
            self.page = self.page + 1;
            self.loadingDataFromServer(self.page, self.incEveryQuery)
        }
    }
}
