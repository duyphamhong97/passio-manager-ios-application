//
//  TimeViewController.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 05/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol OnContinuosTimedDelegate {
    func onContinuosTimeClick()

}

protocol GetDateRangeProtocol {
    func calendar(dateBegin:String, dateEnd:String)
}

class OverviewFilterDateViewController: UIViewController {
    
    let selectTime : UserDefaults = UserDefaults()
//    var delegate : OnContinuosTimedDelegate?

//    @IBAction func btnBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
//    }
    @IBOutlet weak var mView: UIView!
    @IBOutlet weak var mTblTime: UITableView!
    @IBOutlet weak var mBtnFinish: UIButton!
    
    var delegate:GetDateRangeProtocol?
    var time:[String]=["Hôm nay", "Hôm qua", "Tuần này", "Tuần trước", "Tháng này", "Tháng Trước", "Tuỳ chọn ngày"]
    var checkStatus : [Bool] = [
        true, false, false, false, false, false, false
    ]

    var position : String = "Hôm nay"
    
    var dateBegin = ""
    var dateEnd = ""
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mTblTime.delegate = self
        mTblTime.dataSource = self
        
        mView.layer.cornerRadius=4
        mBtnFinish.layer.cornerRadius=4
        formatter.dateFormat = "dd/MM/yyyy"
        let date = Date()
        let result = formatter.string(from: date)
        dateBegin = result
        dateEnd = result
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    @IBAction func btnFinishAction(_ sender: Any) {
//        if position == "Tuỳ chọn ngày"{
//            let date = storyboard?.instantiateViewController(withIdentifier: "CalendarSelectViewController") as! CalendarSelectViewController
//
//            present(date, animated: true, completion: nil)
//        }
//
//        delegate?.onContinuosTimeClick()
//        selectTime.set(position, forKey: "time")
//        let view = storyboard?.instantiateViewController(withIdentifier: "ReportFilterOptionViewController") as! ReportFilterOptionViewController
//        present(view, animated: true, completion: nil)
//
//    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnFinish(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        delegate?.calendar(dateBegin: dateBegin, dateEnd: dateEnd)
    }
    

//    @IBAction func btnFinishAction(_ sender: Any) {
//        if position == "Tuỳ chọn ngày"{
//            let date = storyboard?.instantiateViewController(withIdentifier: "date") as! ViewController
//
//            present(date, animated: true, completion: nil)
//        }else{
//
//        }
//        delegate?.onContinuosTimeClick()
//        selectTime.set(position, forKey: "time")
//        let view = storyboard?.instantiateViewController(withIdentifier: "ReportFilterOptionViewController") as! ReportFilterOptionViewController
//        present(view, animated: true, completion: nil)
//    }
}

extension OverviewFilterDateViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return time.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mTblTime.dequeueReusableCell(withIdentifier: "cell") as! ReportTableViewCell
        cell.mLblDiscount.text = time[indexPath.row]
        
        if checkStatus[indexPath.row] == true {
            cell.mImgCheck.image = UIImage(named: "ic_check")
            
        }else{
            cell.mImgCheck.image = UIImage(named: "ic_uncheck")
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<checkStatus.count {
            checkStatus[i]=false
        }
        checkStatus[indexPath.row] = true
        position = time[indexPath.row]
        
        tableView.reloadData()
        if (indexPath.row != 6) {
            time[6] = "Tuỳ chọn ngày"
        }
        switch indexPath.row {
        case 0:
            let date = Date()
            let result = formatter.string(from: date)
            dateBegin = result
            dateEnd = result
            break
        case 1:
            let date = Calendar.current.date(byAdding: .day, value: -1, to:Date())
            let result = formatter.string(from: date!)
            dateBegin = result
            dateEnd = result
            break
        case 2:
            var date = Date()
            date = date.startOfWeek!
            var result = formatter.string(from: date)
            dateBegin = result
            date = Date()
            date = date.endOfWeek!
            
            if !checkDate(dateEnd: date) {
                let date = Calendar.current.date(byAdding: .day, value: -1, to:Date())
                let result = formatter.string(from: date!)
                dateEnd = result
            }else{
                result = formatter.string(from: date)
                dateEnd = result
            }
            break
        case 3:
            var date = Date()
            date = date.startOfLastWeek!
            var result = formatter.string(from: date)
            dateBegin = result
            date = Date()
            date = date.endOfLastWeek!
            result = formatter.string(from: date)
            dateEnd = result
            break
        case 4:
            var date = Date()
            date = date.startOfMonth!
            var result = formatter.string(from: date)
            dateBegin = result
            date = Date()
            date = date.endOfMonth!
            if !checkDate(dateEnd: date) {
                let date = Calendar.current.date(byAdding: .day, value: -1, to:Date())
                let result = formatter.string(from: date!)
                dateEnd = result
            }else{
                result = formatter.string(from: date)
                dateEnd = result
            }
            break
        case 5:
            var date = Date()
            date = date.startOfLastMonth!
            var result = formatter.string(from: date)
            dateBegin = result
            date = Date()
            date = date.endOfLastMonth!
            result = formatter.string(from: date)
            dateEnd = result
            break
        case 6:
            let calendarSelectViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.CalendarSelectViewController) as! CalendarSelectViewController
            calendarSelectViewController.delegate = self
            self.navigationController?.pushViewController(calendarSelectViewController, animated: true)
        default:
            break
        }
    }
    
    func checkDate(dateEnd : Date) -> Bool {
        let date = Date()
        let toDay = date
        if toDay < dateEnd {
            return false
        }else{
            return true
        }
    }
}

extension OverviewFilterDateViewController:GetDateRangeProtocol {
    func calendar(dateBegin: String, dateEnd: String) {
        self.dateBegin = dateBegin
        self.dateEnd = dateEnd
    }
}
