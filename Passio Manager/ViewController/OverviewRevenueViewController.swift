//
//  OverviewRevenueViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/4/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import Charts


class OverviewRevenueViewController: UIViewController {

    @IBOutlet weak var chtRevenue: PieChartView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblOptionOne: UILabel!
    @IBOutlet weak var lblOptionTwo: UILabel!
    @IBOutlet weak var lblOptionThree: UILabel!
    @IBOutlet weak var lblOptionOneValue: UILabel!
    @IBOutlet weak var lblOptionTwoValue: UILabel!
    @IBOutlet weak var lblOptionThreeValue: UILabel!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var btnDetailReport: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var optionOneValue:Int = 0
    var optionTwoValue:Int = 0
    var optionThreeValue:Int = 0
    var point:String = ""
    var dataRevenue = TotalRevenue()
    var dataQuantity = TotalQuantity()
    var checkView:Bool = true;
    
    var dateBeginUserDefault : UserDefaults = UserDefaults()
    var dateEndUserDefault : UserDefaults = UserDefaults()
    
    var dateBegin : String!
    var dateEnd : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialData()
        self.scrollView.addSubview(self.refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initialData() {
        dateBegin = (dateBeginUserDefault.object(forKey: "dateBegin") as! String)
        dateEnd = (dateEndUserDefault.object(forKey: "dateEnd") as! String)
        
        if checkView == true {
            optionOneValue = dataRevenue.atStore
            optionTwoValue = dataRevenue.delivery
            optionThreeValue = dataRevenue.takeAway
            point = "đ"
            initialGraph()
            viewDetail.setRadius(radius: 4)
            btnDetailReport.setRadius(radius: 4)
            
            lblTotal.text = ConvertManager.formatNumberToMoney(data: optionOneValue + optionTwoValue + optionThreeValue, currencyType: "") + " " + point
            lblPlace.text = (StoreManager.getStore()?.storeName)! + "\n" + dateBegin + " - " + dateEnd
            lblOptionOne.text = "Tại cửa hàng (" + String(format: "%.2f", Double(optionOneValue) * 100.0 / Double(optionOneValue + optionTwoValue + optionThreeValue)) + "%)"
            lblOptionTwo.text = "Giao Hàng (" + String(format: "%.2f", Double(optionTwoValue) * 100.0 / Double(optionOneValue + optionTwoValue + optionThreeValue)) + "%)"
            lblOptionThree.text = "Mang Về (" + String(format: "%.2f", Double(optionThreeValue) * 100.0 / Double(optionOneValue + optionTwoValue + optionThreeValue)) + "%)"
            lblOptionOneValue.text = ConvertManager.formatNumberToMoney(data: optionOneValue, currencyType: "") + " " + point
            lblOptionTwoValue.text = ConvertManager.formatNumberToMoney(data: optionTwoValue, currencyType: "") + " " + point
            lblOptionThreeValue.text = ConvertManager.formatNumberToMoney(data: optionThreeValue, currencyType: "") + " " + point
        }else{
            optionOneValue = dataQuantity.atStore
            optionTwoValue = dataQuantity.delivery
            optionThreeValue = dataQuantity.takeAway
            point = "đơn"
            initialGraph()
            viewDetail.setRadius(radius: 4)
            btnDetailReport.setRadius(radius: 4)
            
            lblTotal.text = ConvertManager.formatNumberToMoney(data: optionOneValue + optionTwoValue + optionThreeValue, currencyType: "") + " " + point
            lblPlace.text = (StoreManager.getStore()?.storeName)! + "\n" + dateBegin + " - " + dateEnd
            lblOptionOne.text = "Tại cửa hàng (" + String(format: "%.2f", Double(optionOneValue) * 100.0 / Double(optionOneValue + optionTwoValue + optionThreeValue)) + "%)"
            lblOptionTwo.text = "Giao Hàng (" + String(format: "%.2f", Double(optionTwoValue) * 100.0 / Double(optionOneValue + optionTwoValue + optionThreeValue)) + "%)"
            lblOptionThree.text = "Mang Về (" + String(format: "%.2f", Double(optionThreeValue) * 100.0 / Double(optionOneValue + optionTwoValue + optionThreeValue)) + "%)"
            lblOptionOneValue.text = ConvertManager.formatNumberToMoney(data: optionOneValue, currencyType: "") + " " + point
            lblOptionTwoValue.text = ConvertManager.formatNumberToMoney(data: optionTwoValue, currencyType: "") + " " + point
            lblOptionThreeValue.text = ConvertManager.formatNumberToMoney(data: optionThreeValue, currencyType: "") + " " + point
        }
        
    }
    
    func initialGraph() {
        var pieChartEntry = [PieChartDataEntry]()
        let value1 = PieChartDataEntry(value: Double(optionOneValue))//, label: "Tại cửa hàng")
        pieChartEntry.append(value1)
        let value2 = PieChartDataEntry(value: Double(optionTwoValue))//, label: "Giao hàng")
        pieChartEntry.append(value2)
        let value3 = PieChartDataEntry(value: Double(optionThreeValue))//, label: "Mang về")
        pieChartEntry.append(value3)
        let line1 = PieChartDataSet(values: pieChartEntry, label: "")
        line1.colors = [ConvertManager.hexStringToUIColor(hex: "f7c65e"), ConvertManager.hexStringToUIColor(hex: "db5d52"), ConvertManager.hexStringToUIColor(hex: "4a90e2")]
        line1.drawValuesEnabled = false
        let data = PieChartData()
        data.addDataSet(line1)
        chtRevenue.data = data
        chtRevenue.chartDescription?.text = ""
    }

    @IBAction func btnDetailReport(_ sender: Any) {
        let reportDetailViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ReportDetailViewController) as! ReportDetailViewController
        self.navigationController?.pushViewController(reportDetailViewController, animated: true)
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        
    }
    
    @IBAction func btnShare(_ sender: Any) {
        
    }
    
    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = ConvertManager.hexStringToUIColor(hex: "#a6ce39")
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if NetworkManager.isConnectToInternet() {
            initialData()
        }
        refreshControl.endRefreshing()
    }
}





