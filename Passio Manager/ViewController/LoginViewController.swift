//
//  LoginViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/4/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import JGProgressHUD

class LoginViewController: UIViewController {
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBAction func btnLogin(_ sender: Any) {
        loadDataFromServer()
    }
    
    @IBAction func btnForgotPassword(_ sender: Any) {
        let forgotPassword = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ForgotPasswordViewController) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotPassword, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        btnLogin.layer.cornerRadius = 8
    }

    func loadDataFromServer() {
        let reportApi : LoginBehavior = LoginAPIHelper()
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Đang tải"
        hud.show(in: self.view)
        reportApi.login(username: txtEmail.text!, password: txtPassword.text!).on(
            failed: { error in
                hud.dismiss()
                if !NetworkManager.isConnectToInternet(){
                    self.sendAlert("không thể kết nối mạng")
                }else{
                    self.sendAlert("Email hoặc password sai")
                }
                
        },
            value: { user in
                hud.dismiss()
                self.changePage(user:user)
        }
            ).start()
    }
    
    func changePage(user : User) {
        UserManager.createInfo(info: user)
        let store = StoreManager.getStore()
        if store == nil || (store?.storeName.isEmpty)! {
            let storeListViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.StoreListViewController) as! StoreListViewController
            storeListViewController.intentFrom = "login"
            self.navigationController?.pushViewController(storeListViewController, animated: true)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showHomeController()
        }

    }
    
    func sendAlert(_ message:String) {
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
