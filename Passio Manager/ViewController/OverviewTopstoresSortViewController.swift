//
//  OverviewTopstoresSortViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

protocol sortDetailReportDelegate {
    func getOrderBy(orderBySort orderBySort: Int)
}

class OverviewTopstoresSortViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var lblReportDetail:[String] = ["Số lượng",
                                    "Tổng tiền sau giảm giá","Tên"]
    var lblTopStore:[String] = ["Tên cửa hàng","Hoá đơn bán hàng",
                                "Doanh thu bán hàng"]
    
    var checkTypeSort:Bool = true
    var orderBy = 0
    var delegate: sortDetailReportDelegate?
    
    @IBOutlet weak var tblTopStoresSort: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblTopStoresSort.dataSource = self
        tblTopStoresSort.delegate = self
        //tblTopStoresSort.tableFooterView = UIView()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if checkTypeSort {
            return lblReportDetail.count
        } else {
            return lblTopStore.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let overviewTopstoresSortCell = tblTopStoresSort.dequeueReusableCell(withIdentifier: "TopStoresSortCell") as! OverviewTopstoresSortTableViewCell
        
        if checkTypeSort {
            overviewTopstoresSortCell.lblTitle.text = lblReportDetail[indexPath.row]
        } else {
            overviewTopstoresSortCell.lblTitle.text = lblTopStore[indexPath.row]
        }
        
        
        if checkStatus[indexPath.row] == true {
            overviewTopstoresSortCell.imgCheck.image = UIImage(named: "ic_done")
        }else {
            overviewTopstoresSortCell.imgCheck.image = nil
        }
        
        
        return overviewTopstoresSortCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<checkStatus.count {
            checkStatus[i] = false
        }
        checkStatus[indexPath.row] = true
        tblTopStoresSort.reloadData()

        self.navigationController?.popViewController(animated: true)
        
        if delegate != nil {
            delegate?.getOrderBy(orderBySort: indexPath.row)
        }
    }
    
    var checkStatus:[Bool] = [
        false,false,false
    ]

}
