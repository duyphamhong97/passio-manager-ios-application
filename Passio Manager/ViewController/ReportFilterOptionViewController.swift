//
//  ReportFilterOptionViewController.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 05/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit



class ReportFilterOptionViewController: UIViewController {
    
//    let store : UserDefaults = UserDefaults()
//    let time : UserDefaults = UserDefaults()
    let discount : UserDefaults = UserDefaults()
    let type : UserDefaults = UserDefaults()
    
    @IBOutlet weak var mViewShop: UIView!
    @IBOutlet weak var mViewTime: UIView!
    @IBOutlet weak var mViewDiscount: UIView!
    @IBOutlet weak var mViewTypeProduct: UIView!
    @IBOutlet weak var mBtnFinish: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblUserStore: UILabel!
    @IBOutlet weak var lblChoiceProduct: UILabel!
    @IBOutlet weak var lblChoiceType: UILabel!
    @IBOutlet weak var lblChoiceDiscount: UILabel!
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var dateBegin:String = ""
    var dateEnd:String = ""
    var discountChoice : String = ""
    var typeChoice : String = ""
    let formatter = DateFormatter()
    var dateBeginUserdefault :UserDefaults = UserDefaults()
    var dateEndUserdefault :UserDefaults = UserDefaults()
    
    var storeid :Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        formatter.dateFormat = "dd/MM/yyyy"
        loadViewChoice()
        loadSelectedStore()
        loadSelectTime()
        loadSelectDiscount()
        loadSelectTypeProduct()
    }
    
    func Now() {
        let date = Date()
        let result = formatter.string(from: date)
        dateBegin = result
        dateEnd = result
    }
    
    func loadSelectedStore(){
//        if let storeChoice = store.object(forKey: "store") as? String, let storeID = store.object(forKey: "storeID") as? Int{
//            if storeChoice == ""{
//                lblUserStore.text = "Tất cả cửa hàng"
//            }else{
//                storeid = storeID
//                lblUserStore.text = storeChoice
//            }
//        }else{
//            lblUserStore.text = "Tất cả cửa hàng"
//        }
        lblUserStore.text = (StoreManager.getStore()?.storeName)!
    }
    
    func loadSelectTime() {
//        if let timeChoice : String = self.time.object(forKey: "time") as? String{
//            if timeChoice == "" {
//                lblTime.text = "Hôm nay"
//            }else{
//                lblTime.text = timeChoice
//            }
//        }else{
//             lblTime.text = "Hôm nay"
//        }
        if (dateBegin.isEmpty && dateEnd.isEmpty) {
            lblTime.text = "Hôm nay"
        } else {
            lblTime.text = dateBegin + " - " + dateEnd
        }
        
    }
    
    func loadSelectDiscount() {
        if let discountChoice : String = self.discount.object(forKey: "Discount") as? String{
            if discountChoice == "" {
                lblChoiceDiscount.text = "Trước giảm giá"
            }else{
                lblChoiceDiscount.text = discountChoice
            }
        }else{
            lblChoiceDiscount.text = "Trước giảm giá"
        }
    }
    
    func loadSelectTypeProduct() {
        if let typeChoice : String = self.type.object(forKey: "type") as? String{
            if typeChoice == "" {
                lblChoiceType.text = "Sản phẩm"
            }else{
                lblChoiceType.text = typeChoice
            }
        }else{
            lblChoiceType.text = "Sản phẩm"
        }
    }
    
    func loadViewChoice(){
        mViewShop.layer.cornerRadius=4
        mViewTime.layer.cornerRadius=4
        mViewDiscount.layer.cornerRadius=4
        mViewTypeProduct.layer.cornerRadius=4
//        mViewProduct.layer.cornerRadius=4
        mBtnFinish.layer.cornerRadius=4
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (!dateBegin.isEmpty && !dateEnd.isEmpty) {
            lblTime.text = dateBegin + " - " + dateEnd
        } else {
            lblTime.text = "Hôm nay"
        }
    }
    @IBAction func btnSelectStore(_ sender: Any) {

        let listStore = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.StoreListViewController) as! StoreListViewController
//        listStore.accessToken = accessToken
        listStore.intentFrom = "option"
        listStore.delegate = self
        self.navigationController?.pushViewController(listStore, animated: true)
        
    }
    

    @IBAction func btnSelectTime(_ sender: Any) {
        let time = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewFilterDateViewController) as! OverviewFilterDateViewController
        time.delegate=self
        self.navigationController?.pushViewController(time, animated: true)
    }

    
    @IBAction func btnSelectDiscount(_ sender: Any) {
        let reportView = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ReportViewViewController) as! ReportViewViewController
        self.navigationController?.pushViewController(reportView, animated: true)
    }
    
    @IBAction func btnSelectTypeProduct(_ sender: Any) {
        let product = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ReportFilterViewController) as! ReportFilterViewController
        self.navigationController?.pushViewController(product, animated: true)
    }

    func sendAlert(_ message:String) {
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnTIme(_ sender: Any) {
        let overviewFilterDateViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewFilterDateViewController) as! OverviewFilterDateViewController
        
        overviewFilterDateViewController.delegate = self
        self.navigationController?.pushViewController(overviewFilterDateViewController, animated: true)
    }

    @IBAction func btnFinish(_ sender: Any) {
//        let finish = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ReportViewController) as! ReportViewController
//        finish.intent = "Tuỳ chỉnh"
//        finish.dateBegin = dateBegin
//        finish.dateEnd = dateEnd
//        self.navigationController?.pushViewController(finish, animated: true)
        dateBeginUserdefault.set(dateBegin, forKey: "dateBegin")
        dateEndUserdefault.set(dateEnd, forKey: "dateEnd")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDefault(_ sender: Any) {
        Now()
        lblTime.text = dateBegin + " - " + dateEnd
        lblUserStore.text = (StoreManager.getStore()?.storeName)!
        lblChoiceDiscount.text = "Sau giảm giá"
        
    }
    
}

extension ReportFilterOptionViewController:GetDateRangeProtocol {
    func calendar(dateBegin: String, dateEnd: String) {
        self.dateBegin = dateBegin
        self.dateEnd = dateEnd
    }
}
extension ReportFilterOptionViewController : OnContinuosdDelegate, OnContinuosTimedDelegate, OnContinousDiscountDelegate, OnContinousTypeProductDelegate {
    func getAccessToken(accessToken: String) {
        
    }
    
    func onContinuosClick(storeName: String, storeID: Int) {
        loadSelectedStore()
    }
    
    func OnContinousTypeProductClick() {
        loadSelectTypeProduct()
    }
    
    func OnContinousDiscountClick() {
        loadSelectDiscount()
    }
    
    func onContinuosTimeClick() {
        loadSelectTime()
    }
    

    func onContinuosClick(storeName:String) {
        loadSelectedStore()
    }

}


