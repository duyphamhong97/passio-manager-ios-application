//
//  ReportViewController.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 05/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import SDWebImage
import JGProgressHUD

class ReportViewController: UIViewController {
    let formatter = DateFormatter()
    var dateBegin:String = ""
    var dateEnd:String = ""
    var intent : String = "a"
    var dateBeginUserdefault :UserDefaults = UserDefaults()
    var dateEndUserdefault :UserDefaults = UserDefaults()
    
    let screenHeight = UIScreen.main.bounds.height
    let scrollViewContentHeight = 1200 as CGFloat
    
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var mLblAddress: UILabel!
    @IBOutlet weak var mLblDate: UILabel!
    @IBOutlet weak var mTblReportList: UITableView!
    @IBOutlet weak var mViewReportList: UIView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var btnShare: UIButton!
        
    @IBAction func btnSetting(_ sender: Any) {
        let setting = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ReportFilterOptionViewController) as! ReportFilterOptionViewController
        setting.dateBegin = dateBegin
        setting.dateEnd  = dateEnd
        self.navigationController?.pushViewController(setting, animated: true)
    }
    
    
    var data = [Product]()
    var newData = [Product]()
    var incEveryQuery:Int = 5
    var numberOfElement:Int = 5 // = incEveryQuery
    var page = 1
    var maxOfNumber:Int = 1000
    var orderBy = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipe()
        mTblReportList.delegate=self
        mTblReportList.dataSource=self
        mViewReportList.layer.cornerRadius=4
        mTblReportList.setRadius(radius: 4)
        self.navigationController?.isNavigationBarHidden = true
        scrView.delegate = self
        self.scrView.addSubview(self.refreshControl)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        incEveryQuery = 10
        numberOfElement = 10 // = incEveryQuery
        page = 1
        maxOfNumber = 1000
        orderBy = 1
        data = []
        newData = []
        mLblAddress.text=(StoreManager.getStore()?.storeName)!
        loadingDataFromServer(page,incEveryQuery)
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = ConvertManager.hexStringToUIColor(hex: "#a6ce39")
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if NetworkManager.isConnectToInternet() {
            viewWillAppear(false)
        }
        refreshControl.endRefreshing()
    }
    
    func sendAlert(_ message:String) {
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func swipe() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }

    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
}
extension ReportViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reportDetailViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ReportDetailViewController) as! ReportDetailViewController
        reportDetailViewController.data = data
        self.navigationController?.pushViewController(reportDetailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mTblReportList.dequeueReusableCell(withIdentifier: "cellReport") as! ReportViewTableViewCell
        cell.layer.cornerRadius=4
        if data.count <= indexPath.section {
            return cell
        }
        cell.mLblName.text = data[indexPath.section].name
        cell.mLblPrice.text = ConvertManager.formatNumberToMoney(data: data[indexPath.section].finalAmount, currencyType: Currency.VND)
        cell.mLblQuantity.text =  String(data[indexPath.section].ratio ) + "% • " + String(data[indexPath.section].quantity ) + " SP"
        cell.mImgfood.sd_setImage(with: URL(string: data[indexPath.section].img), placeholderImage: UIImage(named: "img_no_image"))
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        heightView.constant = CGFloat(135 + 96 * data.count) + 5
        return data.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 8))
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }

    func loadingDataFromServer(_ _pageCurrent:Int, _ _pageLimit:Int) {

        let productReportBehavior : ProductReportBehavior = ProductReportAPIHelper()
        
        if ((dateBeginUserdefault.object(forKey: "dateBegin") != nil) && (dateEndUserdefault.object(forKey: "dateEnd") != nil)){
            
            dateBegin = (dateBeginUserdefault.object(forKey: "dateBegin") as? String)!
            dateEnd = (dateEndUserdefault.object(forKey: "dateEnd") as? String)!
            
            if dateEndUserdefault.object(forKey: "dateBegin") as? String == dateEndUserdefault.object(forKey: "dateEnd") as? String{
                DispatchQueue.main.async {
                    self.mLblDate.text = (self.dateBeginUserdefault.object(forKey: "dateBegin") as? String)!
                }
                
            }else{
                DispatchQueue.main.async {
                    self.mLblDate.text = self.dateBegin + " - " + self.dateEnd
                }
            }
            productReportBehavior.getProductReport(startTime: dateBegin, endTime: dateEnd, selectedStoreId: (StoreManager.getStore()?.storeID)!, accessToken: (UserManager.getInfo()?.accessToken)!, skip: (_pageCurrent  - 1) * _pageLimit, take: _pageLimit, OrderByEnum: orderBy).on(
                failed: { error in
            },
                value: { product in
                    for item in product {
                        self.newData.append(item)
                    }
                    if product.isEmpty || product.count < _pageLimit {
                        self.maxOfNumber = self.newData.count
                    }
                    self.data = self.newData
                    DispatchQueue.main.async {
                        self.mTblReportList.reloadData()
                    }
            }
                ).start()
        }else{
            formatter.dateFormat = "dd/MM/yyyy"
            let date = Date()
            let result = formatter.string(from: date)
            dateBegin = result
            dateEnd = result
            if dateBegin == dateEnd{
                mLblDate.text = dateBegin
            }else{
                mLblDate.text = dateBegin+" - "+dateEnd
            }
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Đang tải"
            hud.show(in: self.view)
            productReportBehavior.getProductReport(startTime: dateBegin, endTime: dateEnd, selectedStoreId: (StoreManager.getStore()?.storeID)!, accessToken: (UserManager.getInfo()?.accessToken)!, skip: (_pageCurrent  - 1) * _pageLimit, take: _pageLimit, OrderByEnum: orderBy).on(
                failed: { error in
                    hud.dismiss()
            },
                value: { product in
                    hud.dismiss()
                    for item in product {
                        self.newData.append(item)
                    }
                    if product.isEmpty || product.count < _pageLimit {
                        self.maxOfNumber = self.newData.count
                    }
                    self.data = self.newData
                    DispatchQueue.main.async {
                        self.mTblReportList.reloadData()
                    }
            }
                ).start()
        }
        
    }
    
}

extension ReportViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 5 {
            self.loadMore()
        }
    }
    
    func loadMore() {
        guard !(self.numberOfElement >= self.maxOfNumber) else {
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            self.numberOfElement = min(self.numberOfElement + self.incEveryQuery, self.maxOfNumber)
            self.page = self.page + 1;
            self.loadingDataFromServer(self.page, self.incEveryQuery)
        }
    }
}
