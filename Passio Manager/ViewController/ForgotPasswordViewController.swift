//
//  ForgotPasswordViewController.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 04/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var mViewMail: UIView!
    @IBOutlet weak var mtxtMail: UITextField!
    @IBOutlet weak var mBtnSend: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
       
    }
    
    func setupData(){
        mViewMail.layer.cornerRadius = 4
        mBtnSend.layer.cornerRadius = 4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
