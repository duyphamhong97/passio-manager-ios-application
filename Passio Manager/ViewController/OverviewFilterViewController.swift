//
//  OverviewFilterViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
class OverviewFilterViewController: UIViewController {
    
    @IBOutlet weak var viewFinish: UIButton!
    @IBOutlet weak var viewStore: UIView!
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblStore: UILabel!
    
    let formatter = DateFormatter()
    var dateBegin:String!
    var dateEnd:String!
    var delegate : GetDateRangeProtocol?
    var dateBeginUserDefault : UserDefaults = UserDefaults()
    var dateEndUserDefault : UserDefaults = UserDefaults()

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectStore(_ sender: Any) {
        let storeList = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.StoreListViewController) as! StoreListViewController
        storeList.intentFrom = "filter"
        self.navigationController?.pushViewController(storeList, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "dd/MM/yyyy"
        viewFinish.setRadius(radius: 4)
        viewStore.setRadius(radius: 4)
        viewTime.setRadius(radius: 4)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (StoreManager.getStore()?.storeName)!.isEmpty {
            lblStore.text = "Tất cả cửa hàng"
        }else{
            lblStore.text = (StoreManager.getStore()?.storeName)!
        }
        
        if dateBegin == nil || dateEnd == nil || (dateBegin.isEmpty && dateEnd.isEmpty) {
            lblTime.text = "Hôm nay"
        } else {
            lblTime.text = dateBegin + " - " + dateEnd
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnTime(_ sender: Any) {
        let overviewFilterDateViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewFilterDateViewController) as! OverviewFilterDateViewController
        overviewFilterDateViewController.delegate = self
        self.navigationController?.pushViewController(overviewFilterDateViewController, animated: true)
    }
    
    @IBAction func btnFinish(_ sender: Any) {
//        let report = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewViewController) as! OverviewViewController
//        report.dateBegin = dateBegin
//        report.dateEnd = dateEnd
//        self.navigationController?.pushViewController(report, animated: true)
//        report.delegate=self
        dateBeginUserDefault.set(dateBegin, forKey: "dateBegin")
        dateEndUserDefault.set(dateEnd, forKey: "dateEnd")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDefault(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let date = Date()
        let result = formatter.string(from: date)
        dateBegin = result
        dateEnd = result
        self.lblTime.text = dateBegin + " - " + dateEnd
    }
}
extension OverviewFilterViewController:GetDateRangeProtocol {

    func calendar(dateBegin: String, dateEnd: String) {
        self.dateBegin = dateBegin
        self.dateEnd = dateEnd
    }
}
