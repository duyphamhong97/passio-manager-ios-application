//
//  CalendarSelectViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/10/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarSelectViewController: UIViewController {

    let formatter = DateFormatter()
    var selectedCheckInDate:Date?
    var selectedCheckOutDate:Date?

    var delegate:GetDateRangeProtocol?

    var selectDate : UserDefaults = UserDefaults()

    
    @IBOutlet weak var lblDateStart: UILabel!
    @IBOutlet weak var lblDateEnd: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCalendarView()
        calendarView.scrollToDate(Date())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnFinishAction(_ sender: Any) {
        selectDate.set(lblDateStart.text, forKey: "dateStart")
        selectDate.set(lblDateEnd.text, forKey: "dateEnd")
//        let view = storyboard?.instantiateViewController(withIdentifier: "ReportFilterOptionViewController") as! ReportFilterOptionViewController
//        present(view, animated: true, completion: nil)
        if lblDateStart.text == "Ngày bắt đầu" || lblDateEnd.text == "Ngày kết thúc" {
            sendAlert("Vui lòng chọn đầy đủ ngày bắt đầu và ngày kết thúc")
        } else {
            delegate?.calendar(dateBegin: lblDateStart.text!, dateEnd: lblDateEnd.text!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setupCalendarView() {
        // Setup label
        calendarView.visibleDates { (visibleDates) in
            let date = visibleDates.monthDates.first!.date
            
            self.formatter.dateFormat = "yyyy"
            self.lblYear.text = self.formatter.string(from: date)
            
            self.formatter.dateFormat = "MM"
            self.lblMonth.text = self.formatter.string(from: date)
        }
    }
    
    func sendAlert(_ message:String) {
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
//    @IBAction func btnFinish(_ sender: Any) {
//        if lblDateStart.text == "Ngày bắt đầu" || lblDateEnd.text == "Ngày kết thúc" {
//            sendAlert("Vui lòng chọn đầy đủ ngày bắt đầu và ngày kết thúc")
//        } else {
//            delegate?.calendar(dateBegin: lblDateStart.text!, dateEnd: lblDateEnd.text!)
//            self.navigationController?.popViewController(animated: true)
//        }
//    }
}

extension CalendarSelectViewController:JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let startDate = formatter.date(from: "2000 09 01")
        let endDate = formatter.date(from: "2100 09 01")
        let calendar = NSCalendar.current
        //let parameter = ConfigurationParameters(startDate: startDate!, endDate: endDate!)
        let parameter = ConfigurationParameters(startDate: startDate!, endDate: endDate!, numberOfRows: 5, calendar: calendar, generateInDates: .forAllMonths, generateOutDates: .tillEndOfGrid, firstDayOfWeek: .monday, hasStrictBoundaries: false)
        return parameter
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        sharedFunctionToConfigureCell(myCustomCell: cell, cellState: cellState, date: date)
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        let myCustomCell = cell as! CustomCell
        sharedFunctionToConfigureCell(myCustomCell: myCustomCell, cellState: cellState, date: date)

    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        formatter.dateFormat = "dd/MM/yyyy"
        
        if selectedCheckInDate == nil {
            selectedCheckInDate = date
        } else {
            if selectedCheckOutDate == nil {
                selectedCheckOutDate = date
            } else {
                selectedCheckInDate = date
                selectedCheckOutDate = nil
            }
        }
        
        if selectedCheckInDate != nil && selectedCheckOutDate != nil {
            if selectedCheckInDate! > selectedCheckOutDate! {
                swap(&selectedCheckInDate, &selectedCheckOutDate)
            }
        }
        
        if selectedCheckInDate != nil {
            let selectedDate = formatter.string(from: selectedCheckInDate!)
            lblDateStart.text = selectedDate
            lblDateStart.textColor = Color.dark_sage()
        }
        if selectedCheckOutDate != nil {
            let selectedDate = formatter.string(from: selectedCheckOutDate!)
            lblDateEnd.text = selectedDate
            lblDateEnd.textColor = Color.dark_sage()
        } else {
            let selectedDate = formatter.string(from: selectedCheckInDate!)
            lblDateStart.text = selectedDate
            lblDateStart.textColor = Color.dark_sage()
            lblDateEnd.text = "Ngày kết thúc"
            lblDateEnd.textColor = Color.dark_sage()
            
        }
        calendar.reloadData()
    }
    
    func sharedFunctionToConfigureCell(myCustomCell: CustomCell, cellState: CellState, date: Date) {
        myCustomCell.dateLabel.text = cellState.text
        let calendar = NSCalendar.current
//        if calendar.isDateInToday(date) {
//            myCustomCell.backgroundColor = UIColor.red
//        } else {
//            myCustomCell.backgroundColor = UIColor.white
//        }
        // more code configurations
        // ...
        // ...
        // ...
        if cellState.dateBelongsTo == .thisMonth {
            myCustomCell.dateLabel.textColor = UIColor.black
            let weekDay = calendar.component(.weekday, from: date)
            if weekDay == 1 {
                myCustomCell.dateLabel.textColor = UIColor.red
            } else if weekDay == 7 {
                myCustomCell.dateLabel.textColor = UIColor.blue
            }
        } else {
            myCustomCell.dateLabel.textColor = UIColor.gray
        }
        myCustomCell.backgroundColor = UIColor.clear
        myCustomCell.leftView.backgroundColor = Color.sick_green()
        myCustomCell.rightView.backgroundColor = Color.sick_green()
        myCustomCell.roundView.backgroundColor = Color.sick_green()
        myCustomCell.roundView.layer.cornerRadius = myCustomCell.roundView.frame.size.width/2
        myCustomCell.roundView.clipsToBounds = true

        myCustomCell.leftView.isHidden = true
        myCustomCell.rightView.isHidden = true
        
        if selectedCheckInDate != nil && selectedCheckOutDate != nil && date == selectedCheckInDate && selectedCheckInDate == selectedCheckOutDate {
            myCustomCell.roundView.isHidden = false
            myCustomCell.leftView.isHidden = true
            myCustomCell.rightView.isHidden = true
            myCustomCell.dateLabel.textColor = UIColor.white
            return
        }
        if selectedCheckInDate != nil && selectedCheckOutDate == nil && date == selectedCheckInDate {
            myCustomCell.roundView.isHidden = false
            myCustomCell.leftView.isHidden = true
            myCustomCell.rightView.isHidden = true
            myCustomCell.dateLabel.textColor = UIColor.white
            return
        }
        if selectedCheckInDate == nil && selectedCheckOutDate != nil && date == selectedCheckOutDate! {
            myCustomCell.roundView.isHidden = false
            myCustomCell.leftView.isHidden = true
            myCustomCell.rightView.isHidden = true
            myCustomCell.dateLabel.textColor = UIColor.white
            return
        }
        if selectedCheckInDate != nil && selectedCheckOutDate != nil {
            if date == selectedCheckInDate {
                // round + rightShape
                myCustomCell.roundView.isHidden = false
                myCustomCell.leftView.isHidden = true
                myCustomCell.rightView.isHidden = false
                myCustomCell.dateLabel.textColor = UIColor.white
                let weekDay = calendar.component(.weekday, from: date)
                if weekDay == 1 {
                    myCustomCell.leftView.isHidden = true
                    myCustomCell.rightView.isHidden = true
                }
            } else if date == selectedCheckOutDate {
                myCustomCell.roundView.isHidden = false
                myCustomCell.leftView.isHidden = false
                myCustomCell.rightView.isHidden = true
                myCustomCell.dateLabel.textColor = UIColor.white
                let weekDay = calendar.component(.weekday, from: date)
                if weekDay == 2 {
                    myCustomCell.leftView.isHidden = true
                    myCustomCell.rightView.isHidden = true
                }
            } else if selectedCheckInDate! <= date && date <= selectedCheckOutDate! {
                myCustomCell.dateLabel.textColor = UIColor.white
                let weekDay = calendar.component(.weekday, from: date)
                if weekDay == 1 {
                    myCustomCell.roundView.isHidden = false
                    myCustomCell.leftView.isHidden = false
                    myCustomCell.rightView.isHidden = true
                } else if weekDay == 2 {
                    myCustomCell.roundView.isHidden = false
                    myCustomCell.leftView.isHidden = true
                    myCustomCell.rightView.isHidden = false
                } else {
                    myCustomCell.roundView.isHidden = true
                    myCustomCell.leftView.isHidden = true
                    myCustomCell.rightView.isHidden = true
                    myCustomCell.backgroundColor = Color.sick_green()
                }
                
            } else {
                myCustomCell.leftView.isHidden = true
                myCustomCell.rightView.isHidden = true
                myCustomCell.roundView.isHidden = true
                myCustomCell.backgroundColor = UIColor.clear
            }
        } else {
            myCustomCell.leftView.isHidden = true
            myCustomCell.rightView.isHidden = true
            myCustomCell.roundView.isHidden = true
            myCustomCell.backgroundColor = UIColor.clear
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        
        formatter.dateFormat = "yyyy"
        lblYear.text = formatter.string(from: date)
        
        formatter.dateFormat = "MM"
        lblMonth.text = formatter.string(from: date)
    }
    
    
}
