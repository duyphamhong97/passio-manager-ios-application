//
//  TestViewController.swift
//  Passio Manager
//
//  Created by UniMob on 8/29/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import JGProgressHUD

protocol OnContinuosdDelegate {
    func onContinuosClick(storeName:String, storeID:Int)
}

class StoreListViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var viewBar: UIView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var tbStore: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearch(_ sender: Any) {
        searchBar.isHidden=false
        hideKeyboardWhenTappedAround()
        setSearchBar()
    }
    
    var sectionChoose = false
    var intentFrom :String!
    var  delegate : OnContinuosdDelegate?
    var currentStoreArray = [Store]()

    @IBAction func btnContinueAction(_ sender: Any) {
        let title : String = (btnContinue.titleLabel?.text)!
        if title == "Tiếp tục"{
            StoreManager.createStore(store: currentStoreArray[rowChoose])
            
            let overViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewTabBarViewController) as! OverviewTabBarViewController
            StoreManager.createStore(store: currentStoreArray[rowChoose])
            self.navigationController?.pushViewController(overViewController, animated: true)
        }
        else{
            StoreManager.updateStore(store: currentStoreArray[rowChoose])
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    //var dataOpen:[Bool] = [false, false, false, false, false, false, false, false]
//    var sectionChoose = false
    var rowChoose = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.isHidden=true
        if intentFrom == "login" {
            btnContinue.setTitle("Tiếp tục", for: .normal)
            btnBack.isHidden=true
        }else if intentFrom == "option"{
            btnContinue.setTitle("Áp Dụng", for: .normal)
        }else{
            btnContinue.setTitle("Áp dụng", for: .normal)
        }
        loadingDataFromServer()
       
    }
    
    var data : [Store] = []
    func load(storeList : [Store]) {
        
        data = []
        let store = Store()
        store.storeID = 0
        store.storeName = "Tất cả cửa hàng"
        data.append(store)
        for item in storeList {
            data.append(item)
        }
        currentStoreArray = data
        tbStore.dataSource = self
        tbStore.delegate = self
        tbStore.tableFooterView = UIView()
        btnContinue.setRadius(radius: 4)
    }
    
    func sendAlert(_ message:String) {
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func loadingDataFromServer() {
        let store : GetUserStoreBehavior = GetUserStoreAPIHelper()
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Đang tải"
        hud.show(in: self.view)
        store.getUserStore(accessToken: (UserManager.getInfo()?.accessToken)!).on(
            failed: { error in
                hud.dismiss()
                if !NetworkManager.isConnectToInternet(){
                    self.sendAlert("không có kết nối internet")
                }else{
                    self.sendAlert("fail")
                }
        },
            value: { storeList in
                hud.dismiss()
                self.load(storeList : storeList)
                
        }
            ).start()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension StoreListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentStoreArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbStore.dequeueReusableCell(withIdentifier: "cell") as! StoreListTableViewCell
//        cell.setupData(data: data[indexPath.section][indexPath.row], section: indexPath.section, row:indexPath.row, open:dataOpen[indexPath.section], isChoose: indexPath.section == sectionChoose && indexPath.row == rowChoose)
        
        cell.setupData(data : currentStoreArray[indexPath.section], section : 0, isChoose : indexPath.section == rowChoose)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 4))
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.section == 0 && currentStoreArray[0].storeName == "Tất cả cửa hàng" {

            return 48
        }
        return 88
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
        rowChoose = indexPath.section
        self.tbStore.reloadData()
    }
    
    func setSearchBar() {
        searchBar.delegate=self
        
    }
}
extension StoreListViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentStoreArray = data
            tbStore.reloadData()
            return
        }
        currentStoreArray = data.filter({ store -> Bool in
            guard let text = searchBar.text else{ return false}
            return store.storeName.lowercased().contains(text.lowercased()) || store.address.lowercased().contains(text.lowercased())
        })
        tbStore.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.isHidden=true
    }
}
