//
//  ReportFilterViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

protocol OnContinousTypeProductDelegate {
    func OnContinousTypeProductClick()
}

class ReportFilterViewController: UIViewController{
    let selectType : UserDefaults = UserDefaults()
    var delegate:OnContinousTypeProductDelegate?
    var position : String = "Sản Phẩm"
    
    var lblStr:[String] = [
        "Sản Phẩm","Nhóm Sản Phẩm"
        ]
    
    @IBOutlet weak var tblReportFilter: UITableView!
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblReportFilter.dataSource = self
        tblReportFilter.delegate = self
    }
    
    var checkStatus:[Bool] = [
        true,false
    ]
    
    @IBAction func btnFinishAction(_ sender: Any) {
        selectType.set(position, forKey: "type")
        delegate?.OnContinousTypeProductClick()
//        let view = storyboard?.instantiateViewController(withIdentifier: "ReportFilterOptionViewController") as! ReportFilterOptionViewController
//        present(view, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
extension ReportFilterViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let filterCell = tblReportFilter.dequeueReusableCell(withIdentifier
            : "FilterCell") as! ReportFilterTableViewCell
        
        if checkStatus[indexPath.row] == false {
            filterCell.imgCheckbox.image = UIImage(named: "ic_normal")
        }else {
            filterCell.imgCheckbox.image = UIImage(named: "ic_active-1")
        }
        
        filterCell.lblTitle.text = lblStr[indexPath.row]
        return filterCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<checkStatus.count {
            checkStatus[i] = false
        }
        checkStatus[indexPath.row] = true
        position = lblStr[indexPath.row]
        tblReportFilter.reloadData()
    }
}
