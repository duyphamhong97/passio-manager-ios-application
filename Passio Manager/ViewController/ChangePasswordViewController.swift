//
//  ChangePasswordViewController.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 04/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var mView1: UIView!
    @IBOutlet weak var mView2: UIView!
    @IBOutlet weak var mView3: UIView!
    
    @IBOutlet weak var mBtnChange: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        load()
    }
    
    func load(){
        mView1.layer.cornerRadius = 4
        mView2.layer.cornerRadius = 4
        mView3.layer.cornerRadius = 4
        mBtnChange.layer.cornerRadius = 4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btbBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
