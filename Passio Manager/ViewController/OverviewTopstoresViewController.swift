//
//  OverviewTopstoresViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import JGProgressHUD

class OverviewTopstoresViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
//    func getOrderBy(orderBySort: Int) {
//        sortTopStore = orderBySort
//        loadFromServer()
//    }
    
    
    @IBAction func btnTopSort(_ sender: Any) {
//        let topSortViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewTopstoresSortViewController) as! OverviewTopstoresSortViewController
//        topSortViewController.checkTypeSort = self.checkTypeSort
//        topSortViewController.delegate = self
//        self.navigationController?.pushViewController(topSortViewController, animated: true)
        
        present(controller, animated: true, completion: nil)
    }
    
    var dataTopStore = [TopStoreRevenue]()
    var newData = [TopStoreRevenue]()
    var dateBegin:String = ""
    var dateEnd:String = ""
    var dateBeginUserDefault : UserDefaults = UserDefaults()
    var dateEndUserDefault : UserDefaults = UserDefaults()
    var orderByDes:Bool = false
    var skip:Int = 0
    var take:Int = 10
    var sortTopStore = 0 // 0 ten, 1 : order, 2 doanh thu
    var incEveryQuery:Int = 5
    var numberOfElement:Int = 5 // = incEveryQuery
    var page = 1
    var maxOfNumber:Int = 1000
    
    var controller : UIAlertController!

    
    @IBOutlet weak var tblTopstores: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateBegin = (dateBeginUserDefault.object(forKey: "dateBegin") as? String)!
        dateEnd = (dateEndUserDefault.object(forKey: "dateEnd") as? String)!
        
        tblTopstores.delegate = self
        tblTopstores.dataSource = self
        tblTopstores.tableFooterView = UIView()
        loadFromServer(page, incEveryQuery)
        self.scrollView.addSubview(self.refreshControl)
        
        actionSheet()

    }
    
    func actionSheet() {
        controller = UIAlertController(title: "Sắp xếp theo", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let des = UIAlertAction(title: "Tăng dần", style: UIAlertActionStyle.default) {
            (action) -> Void in
                self.orderByDes = false
                self.sortChange()
        }
        let sec = UIAlertAction(title: "Giảm dần", style: UIAlertActionStyle.default) {
            (action) -> Void in
                self.orderByDes = true
                self.sortChange()
            
        }
        controller.addAction(des)
        controller.addAction(sec)
        
        controller.view.tintColor = ConvertManager.hexStringToUIColor(hex: "a6ce39")

    }
    
    func sortChange() {
        newData = []
        incEveryQuery = 5
        numberOfElement = 5
        page = 1
        maxOfNumber = 1000
        loadFromServer(page, incEveryQuery)
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = ConvertManager.hexStringToUIColor(hex: "#a6ce39")

        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if NetworkManager.isConnectToInternet() {
            newData = []
            incEveryQuery = 5
            page = 1
            maxOfNumber = 1000
            
            self.loadFromServer(self.page, self.incEveryQuery)

        }else{
            sendAlert("không có kết nối internet")
        }
        refreshControl.endRefreshing()
    }
    func sendAlert(_ message:String) {
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataTopStore.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblTopstores.dequeueReusableCell(withIdentifier: "TopstoresCell") as! OverviewTopstoresTableViewCell
        cell.btnNumber.text = String(indexPath.section+1)
        cell.lblTitle.text = dataTopStore[indexPath.section].storeName
        cell.lbl1.text = String(dataTopStore[indexPath.section].totalStoreOrder) + " sp"
        cell.lbl3.text = ConvertManager.formatNumberToMoney(data: dataTopStore[indexPath.section].totalStoreRevenue, currencyType: Currency.VND)
        
        return cell
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadFromServer(_ _pageCurrent:Int, _ _pageLimit:Int) {
        
        let reportTopStoreApi : GetTopStoreReportBehavior = TopStoreReportAPIHelper()
   
        reportTopStoreApi.getTopStore(startTime: dateBegin, endTime: dateEnd, selectedStoreId: (StoreManager.getStore()?.storeID)!, accessToken: (UserManager.getInfo()?.accessToken)!, skip: _pageLimit * (_pageCurrent - 1), take: _pageLimit, orderByDes: orderByDes).on(
                    failed: { error in
                        print(error)
                },
                    value: { reportTopStore in
                        for item in reportTopStore {
                            self.newData.append(item)
                        }
                        if reportTopStore.isEmpty || reportTopStore.count < _pageLimit {
                            self.maxOfNumber = self.newData.count
                        }
                        
                        self.dataTopStore = self.newData
                        DispatchQueue.main.async {
                            self.tblTopstores.reloadData()
                        }

                }
                    ).start()
            }
}

extension OverviewTopstoresViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 5 {
            self.loadMore()
        }
    }
    
    func loadMore() {
        guard !(self.numberOfElement >= self.maxOfNumber) else {
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            self.numberOfElement = min(self.numberOfElement + self.incEveryQuery, self.maxOfNumber)
            self.page = self.page + 1;
            self.loadFromServer(self.page, self.incEveryQuery)
        }
    }
}
