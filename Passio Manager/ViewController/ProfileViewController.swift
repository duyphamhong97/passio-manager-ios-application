//
//  ProfileViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/4/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var imgPersonal: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var PassioHotline: UILabel!
    @IBOutlet weak var PassioEmail: UILabel!
    
    var dateBeginUserDefault : UserDefaults = UserDefaults()
    var dateEndUserDefault : UserDefaults = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblName.text = "Xin chào " + (UserManager.getInfo()?.fullName)!

        viewDetail.layer.cornerRadius = 4
        imgPersonal.layer.cornerRadius = imgPersonal.frame.height / 2.0
        imgPersonal.clipsToBounds = true
        self.navigationController?.isNavigationBarHidden = true
        swipe()
    }
    
    func swipe() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }

    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    @IBAction func btnCall(_ sender: Any) {
        var hotline = self.PassioHotline.text
        hotline = hotline?.replacingOccurrences(of: " ", with: "")
        
        if let url = URL(string: "telprompt://\(hotline)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }

    @IBAction func btnMail(_ sender: Any) {
        if let email = self.PassioEmail.text {
            if let url = URL(string: "mailto://\(email)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }
    
    @IBAction func btnLogOut(_ sender: Any) {
        let logOutViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.LoginViewController) as! LoginViewController
        UserManager.deleteAllInfo()
        StoreManager.deleteAllInfo()
        dateBeginUserDefault.set(nil, forKey: "dateBegin")
        dateEndUserDefault.set(nil, forKey: "dateEnd")
        self.navigationController?.pushViewController(logOutViewController, animated: true)
    }
}
