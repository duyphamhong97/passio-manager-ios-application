                //
//  OverviewViewController.swift
//  Passio Manager
//
//  Created by UniMob on 9/5/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit
import Charts
import JGProgressHUD

class OverviewViewController: UIViewController {

    @IBOutlet weak var chtChart: CombinedChartView!
    @IBOutlet weak var viewTotalMoney: UIView!
    @IBOutlet weak var viewTotalOrder: UIView!
    @IBOutlet weak var viewTotalPay: UIView!
    @IBOutlet weak var viewTotalPayTime: UIView!

    @IBOutlet weak var height1: NSLayoutConstraint!
    @IBOutlet weak var height2: NSLayoutConstraint!
    @IBOutlet weak var height3: NSLayoutConstraint!
    @IBOutlet weak var height4: NSLayoutConstraint!
    @IBOutlet weak var height5: NSLayoutConstraint!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView3: UIImageView!
    @IBOutlet weak var imgView4: UIImageView!
    @IBOutlet weak var imgView5: UIImageView!
    @IBOutlet weak var heightTotal: NSLayoutConstraint!
    @IBOutlet weak var viewImage1: UIView!
    @IBOutlet weak var viewImage2: UIView!
    @IBOutlet weak var viewImage3: UIView!
    @IBOutlet weak var viewImage4: UIView!
    @IBOutlet weak var viewImage5: UIView!

    @IBOutlet weak var lblTotalRevenueTotal: UILabel!
    @IBOutlet weak var imgTotalRevenueTrending: UIImageView!
    @IBOutlet weak var lblTotalRevenueTrending: UILabel!
    @IBOutlet weak var lblTotalRevenueTotalReal: UILabel!
    @IBOutlet weak var lblTotalRevenueDiscount: UILabel!
    @IBOutlet weak var lblTotalRevenueFinal: UILabel!
    @IBOutlet weak var lblTotalRevenueOrderCard: UILabel!
    
    @IBOutlet weak var lblTotalQuantityTotalOrder: UILabel!
    @IBOutlet weak var imgTotalQuantityTrending: UIImageView!
    @IBOutlet weak var lblTotalQuantityTrending: UILabel!
    @IBOutlet weak var lblTotalQuantityAtStore: UILabel!
    @IBOutlet weak var lblTotalQuantityTakeAway: UILabel!
    @IBOutlet weak var lblTotalQuantityDelivery: UILabel!
    @IBOutlet weak var lblTotalQuantityOrderCard: UILabel!
    
    @IBOutlet weak var lblTotalPaymentTotalPayment: UILabel!
    @IBOutlet weak var imgTotalPaymentTrending: UIImageView!
    @IBOutlet weak var lblTotalPaymentTrending: UILabel!
    @IBOutlet weak var lblTotalPaymentCash: UILabel!
    @IBOutlet weak var lblTotalPaymentOrderCard: UILabel!
    @IBOutlet weak var lblTotalPaymentCard: UILabel!
    @IBOutlet weak var lblNameStore: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblTotalPaymentTransactionTotalTransaction: UILabel!
    @IBOutlet weak var imgTotalPaymentTransactionTrending: UIImageView!
    @IBOutlet weak var lblTotalPaymentTransactionTrending: UILabel!
    @IBOutlet weak var lblTotalPaymentTransactionTotal: UILabel!
    @IBOutlet weak var lblTotalPaymentTransactionOrderCardTransaction: UILabel!
    @IBOutlet weak var lblTotalPaymentTransactionCardTransaction: UILabel!
    
    @IBOutlet weak var lblTopStoreName: UILabel!
    @IBOutlet weak var lblTopStoreTotalStoreOrder: UILabel!
    @IBOutlet weak var lblTopStoreTotalStoreRevenue: UILabel!
    
    @IBOutlet weak var viewTopStore: UIView!
    @IBOutlet weak var viewDetailTopStore: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var viewChart: UIView!

    
    var dateBegin:String = ""
    var dateEnd:String = ""
    let formatter = DateFormatter()
    var dataRevenue = TotalRevenue()
    var dataQuantity = TotalQuantity()
    var dataTopStore = TopStoreRevenue()
    var checkView:Bool = true
    var delegate : GetDateRangeProtocol?
    var dateBeginUserDefault : UserDefaults = UserDefaults()
    var dateEndUserDefault : UserDefaults = UserDefaults()

    var screebShot : UIImage?

    
    var height:[CGFloat] = [260, 260, 185, 180, 190]
    var down:[Bool] = [false, false, false, false, false]
    var money = [Double]()
    var number = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        swipe()
        self.navigationController?.isNavigationBarHidden = true
        self.scrollView.addSubview(self.refreshControl)
    }


    override func viewWillAppear(_ animated: Bool) {
        if NetworkManager.isConnectToInternet() {
            loadDataFromServer()
        }else{
            self.sendAlert("không có kết nối internet")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = ConvertManager.hexStringToUIColor(hex: "#a6ce39")
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if NetworkManager.isConnectToInternet() {
            loadDataFromServer()
        }else{
            sendAlert("không có kết nối internet")
        }
        refreshControl.endRefreshing()
    }
    func sendAlert(_ message:String) {
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnOverviewDetail(_ sender: Any) {
        let overviewRevenueViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewRevenueViewController) as! OverviewRevenueViewController
        self.navigationController?.pushViewController(overviewRevenueViewController, animated: true)
        
        dateBeginUserDefault.set(dateBegin, forKey: "dateBegin")
        dateEndUserDefault.set(dateEnd, forKey: "dateEnd")
        
        overviewRevenueViewController.dataRevenue = self.dataRevenue
        checkView = true;
        overviewRevenueViewController.checkView = self.checkView
    }
    
    @IBAction func btnViewTotalOrder(_ sender: Any) {
        let overviewRevenueViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewRevenueViewController) as! OverviewRevenueViewController
        self.navigationController?.pushViewController(overviewRevenueViewController, animated: true)
        
        overviewRevenueViewController.dataQuantity = self.dataQuantity
        checkView = false
        overviewRevenueViewController.checkView = self.checkView
    }
    
    @IBAction func btnTopStores(_ sender: Any) {
        let overviewTopstoresViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewTopstoresViewController) as! OverviewTopstoresViewController
        
        dateBeginUserDefault.set(dateBegin, forKey: "dateBegin")
        dateEndUserDefault.set(dateEnd, forKey: "dateEnd")
        
        self.navigationController?.pushViewController(overviewTopstoresViewController, animated: true)
        
        }
    
    @IBAction func btnSetting(_ sender: Any) {
        let setting = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.OverviewFilterViewController) as! OverviewFilterViewController
        setting.dateBegin = dateBegin
        setting.dateEnd  = dateEnd
//        delegate?.calendar(dateBegin: self.dateBegin, dateEnd: self.dateEnd)
        setting.delegate=self
        self.navigationController?.pushViewController(setting, animated: true)
    }

    @IBAction func btnShare(_ sender: Any) {
        let shareViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ShareViewController) as! ShareViewController
        let renderer = UIGraphicsImageRenderer(size: viewChart.bounds.size)
        let image = renderer.image { ctx in
            viewChart.drawHierarchy(in: viewChart.bounds, afterScreenUpdates: true)
        }
        //screebShot = UIApplication.shared.screenShot
        shareViewController.screenShot = image
        shareViewController.nameStore = lblNameStore.text
        shareViewController.date = lblDate.text
        shareViewController.revenue = lblTotalRevenueTotal.text
        shareViewController.bill = lblTotalQuantityTotalOrder.text
        self.navigationController?.pushViewController(shareViewController, animated: true)
        
    }

    func swipe() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }

    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }

    func loadDataFromServer() {
        
        let reportApi : ReportBehavior = ReportAPIHelper()
        let reportDateApi : GetDateReportBehavior = ReportDateAPIHelper()
        
        lblNameStore.text = (StoreManager.getStore()?.storeName)!
        
        if ((dateBeginUserDefault.object(forKey: "dateBegin") != nil) && (dateEndUserDefault.object(forKey: "dateEnd") != nil)){
            
            dateBegin = (dateBeginUserDefault.object(forKey: "dateBegin") as? String)!
            dateEnd = (dateEndUserDefault.object(forKey: "dateEnd") as? String)!
            
            if (dateBegin == dateEnd){
                lblDate.text = dateBegin
                let hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Đang tải"
                hud.show(in: self.view)
                reportApi.getHourReport(accessToken: (UserManager.getInfo()?.accessToken)!, dateStart: dateBegin, dateEnd: dateEnd, StoreID: (StoreManager.getStore()?.storeID)!).on(
                    failed: { error in
                        hud.dismiss()
                },
                    value: { report in
                        hud.dismiss()
                        self.setupData(report:report)
                }
                    ).start()
            }else{
                lblDate.text = dateBegin+" - "+dateEnd
                let hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Đang tải"
                hud.show(in: self.view)
                reportDateApi.getDateReport(accessToken: (UserManager.getInfo()?.accessToken)!, dateStart: dateBegin, dateEnd: dateEnd, StoreID: (StoreManager.getStore()?.storeID)!).on(
                    failed: { error in
                        hud.dismiss()
                },
                    value: { reportDate in
                        hud.dismiss()
                        self.setupDataDate(report: reportDate)
                }
                    ).start()
            }
        }else{
            
            formatter.dateFormat = "dd/MM/yyyy"
            let date = Date()
            let result = formatter.string(from: date)
            dateBegin = result
            dateEnd = result
            if dateBegin == dateEnd{
                lblDate.text = dateBegin
            }else{
                lblDate.text = dateBegin+" - "+dateEnd
            }
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Đang tải"
            hud.show(in: self.view)
            reportApi.getHourReport(accessToken: (UserManager.getInfo()?.accessToken)!, dateStart: dateBegin, dateEnd: dateEnd, StoreID: (StoreManager.getStore()?.storeID)!).on(
                failed: { error in
                    hud.dismiss()
            },
                value: { report in
                    hud.dismiss()
                    self.setupData(report: report)
            }
                ).start()
        }
        
    }
    
    func setupDataDate(report:ReportDate) {
        money = []
        number = []
        for item in report.dataChart.price {
            money.append(Double(item))
        }
        for item in report.dataChart.quantity {
            number.append(Double(item))
        }
        let defaults = UserDefaults.standard
        defaults.set(report.dataChart.time, forKey: "timeRange")
        DispatchQueue.main.async {
            self.updateGraph()
        }
        
        DispatchQueue.main.async {
            self.lblTotalRevenueTotal.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.total, currencyType: " đ")
            self.lblTotalRevenueTotalReal.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.totalReal, currencyType: " đ")
            self.lblTotalRevenueDiscount.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.discount, currencyType: " đ")
            self.lblTotalRevenueFinal.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.final, currencyType: " đ")
            self.lblTotalRevenueOrderCard.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.orderCard, currencyType: " đ")
            self.dataRevenue = report.totalRevenue
        }
        
        DispatchQueue.main.async {
            self.lblTotalQuantityTotalOrder.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.totalOrder, currencyType: " đơn")
            self.lblTotalQuantityAtStore.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.atStore, currencyType: " đơn")
            self.lblTotalQuantityTakeAway.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.takeAway, currencyType: " đơn")
            self.lblTotalQuantityDelivery.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.delivery, currencyType: " đơn")
            self.lblTotalQuantityOrderCard.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.orderCard, currencyType: " đơn")
            self.dataQuantity = report.totalQuantity
        }
        
        DispatchQueue.main.async {
            self.lblTotalPaymentTotalPayment.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPayment, currencyType: " đ")
            self.lblTotalPaymentCash.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPaymentCash, currencyType: " đ")
            self.lblTotalPaymentOrderCard.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPaymentOrderCard, currencyType: " đ")
            //            self.lblTotalPaymentCard.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPaymentCard, currencyType: " đ")
        }
        
        DispatchQueue.main.async {
            self.lblTotalPaymentTransactionTotalTransaction.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.totalTransaction, currencyType: " lượt")
            self.lblTotalPaymentTransactionTotal.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.totalTransaction, currencyType: " lượt")
            self.lblTotalPaymentTransactionOrderCardTransaction.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.orderCardTransaction, currencyType: " luợt")
            self.lblTotalPaymentTransactionCardTransaction.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.cardTransaction, currencyType: " lượt")
        }
        
        if report.topStoreRevenue.totalStoreOrder ==  0 {
            viewTopStore.isHidden = true
            updateHeight()
        }else {
            DispatchQueue.main.async {
                self.viewTopStore.isHidden = false
                self.lblTopStoreName.text = report.topStoreRevenue.storeName
                self.lblTopStoreTotalStoreOrder.text = ConvertManager.formatNumberToMoney(data: report.topStoreRevenue.totalStoreOrder, currencyType: " đơn")
                self.lblTopStoreTotalStoreRevenue.text = ConvertManager.formatNumberToMoney(data: report.topStoreRevenue.totalStoreRevenue, currencyType: " đ")
                self.updateHeight()
            }
        }
        updateHeight()
        
    }
    
    func setupData(report:Report) {
        money = []
        number = []
        for item in report.dataChart.price {
            money.append(Double(item))
        }
        for item in report.dataChart.quantity {
            number.append(Double(item))
        }
        let defaults = UserDefaults.standard
        defaults.set(report.dataChart.time, forKey: "timeRange")
        DispatchQueue.main.async {
            self.updateGraph()
        }
        
        DispatchQueue.main.async {
            self.lblTotalRevenueTotal.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.total, currencyType: " đ")
            self.lblTotalRevenueTotalReal.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.totalReal, currencyType: " đ")
            self.lblTotalRevenueDiscount.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.discount, currencyType: " đ")
            self.lblTotalRevenueFinal.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.final, currencyType: " đ")
            self.lblTotalRevenueOrderCard.text = ConvertManager.formatNumberToMoney(data: report.totalRevenue.orderCard, currencyType: " đ")
            self.dataRevenue = report.totalRevenue
        }
        
        DispatchQueue.main.async {
            self.lblTotalQuantityTotalOrder.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.totalOrder, currencyType: " đơn")
            self.lblTotalQuantityAtStore.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.atStore, currencyType: " đơn")
            self.lblTotalQuantityTakeAway.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.takeAway, currencyType: " đơn")
            self.lblTotalQuantityDelivery.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.delivery, currencyType: " đơn")
            self.lblTotalQuantityOrderCard.text = ConvertManager.formatNumberToMoney(data: report.totalQuantity.orderCard, currencyType: " đơn")
            self.dataQuantity = report.totalQuantity
        }
        
        DispatchQueue.main.async {
            self.lblTotalPaymentTotalPayment.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPayment, currencyType: " đ")
            self.lblTotalPaymentCash.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPaymentCash, currencyType: " đ")
            self.lblTotalPaymentOrderCard.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPaymentOrderCard, currencyType: " đ")
//            self.lblTotalPaymentCard.text = ConvertManager.formatNumberToMoney(data: report.totalPayment.totalPaymentCard, currencyType: " đ")
        }
        
        DispatchQueue.main.async {
            self.lblTotalPaymentTransactionTotalTransaction.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.totalTransaction, currencyType: " lượt")
            self.lblTotalPaymentTransactionTotal.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.totalTransaction, currencyType: " lượt")
            self.lblTotalPaymentTransactionOrderCardTransaction.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.orderCardTransaction, currencyType: " luợt")
            self.lblTotalPaymentTransactionCardTransaction.text = ConvertManager.formatNumberToMoney(data: report.totalPaymentTransaction.cardTransaction, currencyType: " lượt")
        }
        if report.topStoreRevenue.totalStoreOrder ==  0 {
            DispatchQueue.main.async {
                self.viewTopStore.isHidden = true
                self.updateHeight()
            }
        }else {
            DispatchQueue.main.async {
                self.viewTopStore.isHidden = false
                self.lblTopStoreName.text = report.topStoreRevenue.storeName
                self.lblTopStoreTotalStoreOrder.text = ConvertManager.formatNumberToMoney(data: report.topStoreRevenue.totalStoreOrder, currencyType: " đơn")
                self.lblTopStoreTotalStoreRevenue.text = ConvertManager.formatNumberToMoney(data: report.topStoreRevenue.totalStoreRevenue, currencyType: " đ")
                self.updateHeight()
            }
        }
//        report.totalRevenue.total = self.data
//        print(data)
        
    }
    
    func updateView() {
        viewTotalMoney.setRadius(radius: 10)
        viewTotalOrder.setRadius(radius: 10)
        viewTotalPay.setRadius(radius: 10)
        viewTotalPayTime.setRadius(radius: 10)
        viewTopStore.setRadius(radius: 10)
        viewImage1.layer.cornerRadius = viewImage1.frame.height / 2.0
        viewImage1.clipsToBounds = true
        viewImage2.layer.cornerRadius = viewImage2.frame.height / 2.0
        viewImage2.clipsToBounds = true
        viewImage3.layer.cornerRadius = viewImage3.frame.height / 2.0
        viewImage3.clipsToBounds = true
        viewImage4.layer.cornerRadius = viewImage4.frame.height / 2.0
        viewImage4.clipsToBounds = true
        viewImage5.layer.cornerRadius = viewImage5.frame.height / 2.0
        viewImage5.clipsToBounds = true
        
        updateHeight()
    }
    
    func updateHeight() {
        if down[0] {
            height1.constant = height[0]
            imgView1.image = UIImage(named: "ic_arrow_up")
        } else {
            height1.constant = 72
            imgView1.image = UIImage(named: "ic_arrow_down")
        }
        if down[1] {
            height2.constant = height[1]
            imgView2.image = UIImage(named: "ic_arrow_up")
        } else {
            height2.constant = 72
            imgView2.image = UIImage(named: "ic_arrow_down")
        }
        if down[2] {
            height3.constant = height[2]
            imgView3.image = UIImage(named: "ic_arrow_up")
        } else {
            height3.constant = 72
            imgView3.image = UIImage(named: "ic_arrow_down")
        }
        if down[3] {
            height4.constant = height[3]
            imgView4.image = UIImage(named: "ic_arrow_up")
        } else {
            height4.constant = 72
            imgView4.image = UIImage(named: "ic_arrow_down")
        }
        if down[4] {
            height5.constant = height[4]
            imgView5.image = UIImage(named: "ic_arrow_up")
        } else {
            height5.constant = 72
            imgView5.image = UIImage(named: "ic_arrow_down")
        }
        var h:CGFloat = 600
        
        if viewTopStore.isHidden {
            height5.constant = 0
            if down[4] {
                h = h - height[4]
            } else {
                h = h - 72
            }
        }

        
        for i in 0..<down.count {
            if down[i] {
                h = h + height[i]
            } else {
                h = h + 72
            }
        }
        heightTotal.constant = h
    }
    
    func updateGraph() {
        var lineChartEntry = [ChartDataEntry]()
        for i in 0..<money.count {
            let value = ChartDataEntry(x:Double(i), y:money[i])
            lineChartEntry.append(value)
        }
        let line = LineChartDataSet(values: lineChartEntry, label: "Doanh thu")
        line.colors = [Color.pale_grey_two()]
        line.lineWidth = 3
        line.fillColor = Color.white()
        line.mode = .horizontalBezier
        line.drawValuesEnabled = false
        line.drawCirclesEnabled = true
        line.circleColors = [Color.white()]
        line.circleHoleColor = Color.sick_green()
        line.circleRadius = 6
        line.circleHoleRadius = 3
        line.axisDependency = .left
        
        let xAxis                           = chtChart.xAxis
        xAxis.labelPosition                 = .bottom
        xAxis.axisMinimum                   = 0.0
        xAxis.granularity                   = 1.0
        xAxis.valueFormatter                = BarChartFormatter()
        xAxis.spaceMin                      = 1.0
        xAxis.centerAxisLabelsEnabled       = false
        xAxis.labelTextColor = Color.white()
        xAxis.drawGridLinesEnabled = false
        
        // MARK: leftAxis
        let leftAxis                        = chtChart.leftAxis
        leftAxis.drawGridLinesEnabled       = true
        leftAxis.axisMinimum                = 0
        leftAxis.axisMaximum                = money.max()! * 1.1
        leftAxis.valueFormatter             = leftAxisFormatter()
        leftAxis.labelTextColor = Color.white()
        leftAxis.drawAxisLineEnabled = false
//        var value = money.max()!
//        while value / 10 > number.max()! {
//            value = value / 10
//        }
        // MARK: rightAxis
        let rightAxis                       = chtChart.rightAxis
        rightAxis.drawGridLinesEnabled      = false
        rightAxis.axisMinimum               = 0
        rightAxis.axisMaximum               = number.max()! * 1.1
        rightAxis.labelTextColor = Color.white()

        var barChartEntry = [BarChartDataEntry]()
        for i in 0..<number.count {
            let value = BarChartDataEntry(x:Double(i), y:number[i] * leftAxis.axisMaximum / rightAxis.axisMaximum)
            barChartEntry.append(value)
        }
        let bar = BarChartDataSet(values: barChartEntry, label: "Hoá đơn")
        bar.colors = [Color.C649e1b()]
        bar.drawValuesEnabled = false
        
        let data = CombinedChartData()
        data.barData = BarChartData(dataSets: [bar])
        data.lineData = LineChartData(dataSets: [line])
        data.barData.barWidth = 0.75
        chtChart.data = data
        chtChart.chartDescription?.text = ""
        
    }
    
    public class BarChartFormatter: NSObject, IAxisValueFormatter {
        //var months: [String]! = ["9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00"]
        public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            let defaults = UserDefaults.standard
            let months = defaults.object(forKey: "timeRange") as? [String] ?? []
            let modu =  Double(value).truncatingRemainder(dividingBy: Double(months.count))
            if Int(modu) >= months.count || Int(modu) < 0 {
                return "???"
            }
            return months[ Int(modu) ]
        }
    }
    
    public class leftAxisFormatter: NSObject, IAxisValueFormatter {
        public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            var item:Double = value
            if item >= 1000000000 {
                item = item / 1000000000
                let ans = String(format: "%.2f", item) + " tỷ"
                if ans == String(format: "%.0f", item) + ".00" + " tỷ" {
                    return String(format: "%.0f", item) + " tỷ"
                }
                return ans
            }
            if item >= 1000000 {
                item = item / 1000000
                let ans = String(format: "%.1f", item) + "tr"
                if ans == String(format: "%.0f", item) + ".0" + "tr" {
                    return String(format: "%.0f", item) + "tr"
                }
                return ans
            }
            if item >= 1000 {
                item = item / 1000
                return String(format: "%.0f", item) + "k"
            }
            return String(format: "%.0f", value)
        }
    }
    
    @IBAction func btnView1(_ sender: Any) {
        down[0] = !down[0]
        updateHeight()
    }
    
    @IBAction func btnView2(_ sender: Any) {
        down[1] = !down[1]
        updateHeight()
    }
    
    @IBAction func btnView3(_ sender: Any) {
        down[2] = !down[2]
        updateHeight()
    }
    
    @IBAction func btnView4(_ sender: Any) {
        down[3] = !down[3]
        updateHeight()
    }
    
    @IBAction func btnView5(_ sender: Any) {
        down[4] = !down[4]
        updateHeight()
    }
}
extension OverviewViewController : GetDateRangeProtocol{
    
    func calendar(dateBegin: String, dateEnd: String) {
        self.dateBegin = dateBegin
        self.dateEnd = dateEnd
    }
}
//extension CALayer {
//    var screenShot: UIImage?  {
//        let scale = UIScreen.main.scale
//        UIGraphicsBeginImageContextWithOptions(frame.size, false, scale)
//        if let context = UIGraphicsGetCurrentContext() {
//            render(in: context)
//            let screenshot = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//            return screenshot
//        }
//        return nil
//    }
//}
//extension UIApplication {
//    var screenShot: UIImage?  {
//        return keyWindow?.layer.screenShot
//    }
//}
