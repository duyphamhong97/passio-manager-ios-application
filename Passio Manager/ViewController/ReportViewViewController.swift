//
//  ReportViewController.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 05/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

protocol OnContinousDiscountDelegate {
    func OnContinousDiscountClick()
}

class ReportViewViewController: UIViewController {
    let selectDiscount : UserDefaults = UserDefaults()
    var position : String = "Trước giảm giá"
    var delegate : OnContinousDiscountDelegate?

    @IBOutlet weak var mBtnFinish: UIButton!
    @IBOutlet weak var mview: UIView!
    @IBOutlet weak var mTblDiscount: UITableView!
    var check:[String]=["Trước giảm giá", "Sau giảm giá"]
    var checkStatus : [Bool] = [
        true, false
    ]
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mTblDiscount.delegate=self
        mTblDiscount.dataSource=self
        mview.layer.cornerRadius=4
        mBtnFinish.layer.cornerRadius=4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnFinishAction(_ sender: Any) {
        selectDiscount.set(position, forKey: "Discount")
//        let ReportFilterOptionViewController = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.ReportFilterOptionViewController) as! ReportFilterOptionViewController
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension ReportViewViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return check.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mTblDiscount.dequeueReusableCell(withIdentifier: "cell") as! ReportTableViewCell
        cell.mLblDiscount.text = check[indexPath.row]
        
        if checkStatus[indexPath.row] == true {
            cell.mImgCheck.image = UIImage(named: "ic_check")
        }else{
             cell.mImgCheck.image = UIImage(named: "ic_uncheck")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<checkStatus.count {
            checkStatus[i]=false
        }
        
        checkStatus[indexPath.row] = true
        position = check[indexPath.row]
        
        tableView.reloadData()
    }
}
