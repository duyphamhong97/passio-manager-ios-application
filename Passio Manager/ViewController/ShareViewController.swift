//
//  ShareViewController.swift
//  Passio Manager
//
//  Created by Nguyễn Đình Vương on 20/09/2018.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var imgScreenShot: UIImageView!
    @IBOutlet weak var imgChart: UIImageView!
    @IBOutlet weak var lblRevenue: UILabel!
    @IBOutlet weak var lblBill: UILabel!
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet var viewScreen: UIView!
    
    var nameStore :String!
    var date : String!
    var revenue : String!
    var bill : String!
    var screenShot : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgChart.image = screenShot
        viewTotal.setRadius(radius: 4)
        share()
        
        lblRevenue.text = revenue + " đ"
        lblBill.text = bill + " đơn"
        lblStoreName.text = nameStore
        lblDate.text = date
        
    }
    
    func share() {
        let renderer = UIGraphicsImageRenderer(size: viewScreen.bounds.size)
        let image = renderer.image { ctx in
            viewScreen.drawHierarchy(in: viewScreen.bounds, afterScreenUpdates: true)
        }
        let share = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            if let popoverPresentationController = share.popoverPresentationController{
                
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = self.view.bounds
        }
        
        self.present(share, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
