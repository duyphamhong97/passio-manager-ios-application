//
//  Color.swift
//  Passio Manager
//
//  Created by UniMob on 8/30/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import UIKit

class Color {
    
    static func dark_sage() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#53584e")
    }
    
    static func charcoal() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#363e2e")
    }
    
    static func white() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#ffffff")
    }
    
    static func pale_grey_two() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#f1f3f6")
    }
    
    static func C6ca617() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#6ca617")
    }
    
    static func pastel_red() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#db5d52")
    }
    
    static func light_mustard() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#f7c65e")
    }
    
    static func sick_green() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#a6ce39")
    }
    
    static func C649e1b() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#649e1b")
    }
    
    static func cool_grey() -> UIColor {
        return ConvertManager.hexStringToUIColor(hex: "#b0b1b5")
    }
}
