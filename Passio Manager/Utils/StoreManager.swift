//
//  StoreManager.swift
//  Passio Manager
//
//  Created by UniMob on 9/13/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import RealmSwift

class StoreManager {
    
    static func getStore() -> Store? {
        let realm = try! Realm()
        return realm.objects(Store.self).first
    }
    
    static func createStore(store:Store) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(store)
        }
    }
    
    static func updateStore(store:Store) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(store, update: true)
        }
    }
    
    static func deleteAllInfo(obj:Object) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(obj)
        }
    }
    
    static func deleteAllInfo() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
}
