//
//  DateManager.swift
//  Passio Coffee
//
//  Created by UniMob on 7/12/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import UIKit

class ConvertManager {
    
    static func formatNumberToMoney(data:Int, currencyType:String) -> String {
        var ans:String = ""
        var hs:Int=0
        var number:Int = data
        if number == 0 {
            return "0" + currencyType
        }
        while number != 0 {
            if (hs % 3 == 0 && hs != 0) {
                ans = String(number % 10) + "." + ans
            } else {
                ans = String(number % 10) + ans
            }
            number = number / 10
            hs = hs + 1
        }
        return ans + currencyType
    }
    
    static func checkDate(date data:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
//        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date:NSDate = dateFormatter.date(from: data)! as NSDate
        let dateOfWeek:[String] = ["", "Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"]
        
        dateFormatter.dateFormat = "HH:mm:ss"
        var ans:String = dateFormatter.string(from: date as Date)
        
        let calendar = NSCalendar.current
        if calendar.isDateInToday(date as Date) {
            ans += ", Hôm nay"
        } else if calendar.isDateInYesterday(date as Date) {
            ans += ", Hôm qua"
        } else {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            let myCalendar = Calendar(identifier: .gregorian)
            let weekDay = myCalendar.component(.weekday, from: date as Date)
            ans += " - " + dateOfWeek[weekDay] + ", " + dateFormatter.string(from: date as Date)
        }
        return ans
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
