//
//  Constant.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 6/26/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation


class AppURL {
//    static let Host             = "http://apimobile.wisky.vn/MobileApi/" //main
//    static let Host             = "http://115.165.166.32:30780/MobileApi/" //dev
    static let Host             = "http://115.165.166.32:30779/MobileReportApi/"
    
    static let GetHourReport = Host + "getHourReport"
    static let login = Host + "Login"
    static let getProductReport = Host + "GetProductReport"
    static let GetUserStore = Host + "GetStoreUsersJson"
    static let GetDateReport = Host + "GetDateReport"
    static let GetTopstore = Host + "GetTopStoreReport"
}

//class AccessToken {
//    static let getAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.MTI4MjU6NjM2NjYxNDE5NzI3OTg5MTY0.QiYQ3aEXo-1arqMdP8-Z-30vKiQJETAys6IgBJVSZQY"
//
//}

let BrandId = "1"

class AppStoryboard {
    static let Main = "Main"
}

class AppIdentifier {
    static let LoginViewController = "LoginViewController"
    static let ProfileViewController = "ProfileViewController"
    static let ForgotPasswordViewController = "ForgotPasswordViewController"
    static let HelpViewController = "HelpViewController"
    static let ChangePasswordViewController = "ChangePasswordViewController"
    static let CalendarSelectViewController = "CalendarSelectViewController"
    static let StoreListViewController = "StoreListViewController"
    static let OverviewFilterDateViewController = "OverviewFilterDateViewController"
    static let ReportViewViewController = "ReportViewViewController"
    static let ReportFilterViewController = "ReportFilterViewController"
    static let OverviewViewController = "OverviewViewController"
    static let OverviewFilterViewController = "OverviewFilterViewController"
    static let ReportViewController = "ReportViewController"
    static let OverViewController = "OverviewController"
    static let OverviewRevenueViewController = "OverviewRevenueViewController"
    static let OverviewTopstoresViewController = "OverviewTopstoresViewController"
    static let ReportDetailViewController = "ReportDetailViewController"
    static let OverviewTabBarViewController = "OverviewTabBarViewController"
    static let ReportFilterOptionViewController = "ReportFilterOptionViewController"
    static let OverviewTopstoresSortViewController = "OverviewTopstoresSortViewController"

    static let ShareViewController = "ShareViewController"

}

class Currency {
    static let VND = " đ"
    static let point = " điểm"
    static let passioPoint = " P"
}

class ColorPallet {
    static let sickGreen = ConvertManager.hexStringToUIColor(hex: "#a6ce39")
    static let lightMustard = ConvertManager.hexStringToUIColor(hex: "#f7c65e")
    static let pastelRed = ConvertManager.hexStringToUIColor(hex: "#db5d52")
    static let charcoal = ConvertManager.hexStringToUIColor(hex: "#363e2e")
    static let silver = ConvertManager.hexStringToUIColor(hex: "#b5b7b7")
    static let darkSage = ConvertManager.hexStringToUIColor(hex: "#53584e")
    static let darkSkyBlue = ConvertManager.hexStringToUIColor(hex: "#4a90e2")
    static let denimBlue = ConvertManager.hexStringToUIColor(hex: "#3b579d")
}

class RankName {
    static let rankName = ["Green": "mới", "Silver": "bạc", "Gold": "vàng"]
}

