//
//  AppHelper.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 6/26/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import AVFoundation
import SystemConfiguration
import UIKit

private let _singletonInstance = AppHelper()

class AppHelper {
    
    class var sharedInstance: AppHelper {
            return _singletonInstance
    }
    
    static func loadViewControllerFromStoryboard(storyboardName: String, bundle: Bundle? = nil, withIdentifier identifier: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: bundle)
        
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
    
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    static func writeImageIntoDisk(image: UIImage, imageName: String) -> URL? {
        var fileName:URL?
        if let data = UIImageJPEGRepresentation(image, 1) {
            fileName = self.getDocumentsDirectory().appendingPathComponent(imageName)
            try? data.write(to: fileName!)
        }
        return fileName
    }
    
    static func loadImageFromRemoteURL(url: URL) -> UIImage {
        var image:UIImage?
        if let data = try? Data(contentsOf: url) {
            image = UIImage(data: data)!
        }
        return image!
    }
    
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio = targetSize.width / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if (widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * heightRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func generateBarcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 20, y: 20)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    static func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 100, y: 100)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    static func isRemoteURL(url: URL) -> Bool {
        let str = url.absoluteString
        let startIndex = String.Index(encodedOffset: 0)
        let endIndex = String.Index(encodedOffset: 4)
        let subString = String(str[startIndex..<endIndex])
        if subString != "http" {
            return false
        } else {
            return true
        }
    }
    
    static func swipeToPop(viewController: UIViewController) {
        viewController.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        viewController.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
}

extension UIScreen {
    public func animateBrightness(to newValue: CGFloat, duration: TimeInterval, ticksPerSecond: Double = 60) {
        let startingBrightness = UIScreen.main.brightness
        let delta = newValue - startingBrightness
        let totalTicks = Int(ticksPerSecond * duration)
        let changePerTick = delta / CGFloat(totalTicks)
        let delayBetweenTicks = 1 / ticksPerSecond
        
        brightness += changePerTick
        
        DispatchQueue.global(qos: .userInteractive).async {
            var previousValue = self.brightness
            
            for _ in 2...totalTicks {
                Thread.sleep(forTimeInterval: delayBetweenTicks)
                
                guard previousValue == self.brightness else {
                    // Value has changed since thread went to sleep 😴
                    return
                }
                let nextValue = min(1, self.brightness + changePerTick)
                
                self.brightness = nextValue
                
                // Don't use `nextValue` here as iOS appears to do some rounding, so
                // the actual value of `self.brightness` may differ from `nextValue`
                previousValue = self.brightness
            }
        }
    }
}
