//
//  UserManager.swift
//  Passio Manager
//
//  Created by UniMob on 9/13/18.
//  Copyright © 2018 Passio Coffee. All rights reserved.
//

import Foundation
import RealmSwift

class UserManager {
    
    static func setDefaultRealmForUser(username: String) {
        var config = Realm.Configuration()
        
        // Use the default directory, but replace the filename with the username
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("\(username).realm")
        
        // Set this as the configuration used for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
    
    static func getInfo() -> User? {
        let realm = try! Realm()
        return realm.objects(User.self).first
    }
    
    static func createInfo(info:User) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(info)
        }
    }
    
    static func deleteAllInfo(obj:Object) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(obj)
        }
    }
    
    static func deleteAllInfo() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    static func updateAccessToken(accessToken:String) {
        let realm = try! Realm()
        let info = self.getInfo()
        try! realm.write {
            info?.accessToken = accessToken
        }
    }
    //    static func updateInfoToServer(name:String, phone:String, email:String) {
    //        let realm = try! Realm()
    //        var info = self.getInfo()
    //
    //        if info == nil {
    //            try! realm.write {
    //                realm.create(PersonalInformation.self, value: ["name": name, "phone": phone, "email": email, "firstLogin": false], update: true)
    //            }
    //        } else {
    //            try! realm.write{
    //                info?.name = name
    //                info?.phone = phone
    //                info?.email = email
    //            }
    //        }
    //    }
    
//    static func updateInfoFirstTime(name:String, phone:String, email:String) {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.create(PersonalInformation.self, value: ["name": name, "phone": phone, "email": email, "firstLogin": false], update: true)
//        }
//    }
//
//    static func updateInfo(name:String, phone:String, email:String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.name = name
//            info?.phone = phone
//            info?.email = email
//            info?.firstLogin = false
//        }
//    }
//
//    static func updateAvatar(picUrl: String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        if info != nil {
//            try! realm.write {
//                info?.picUrl = picUrl
//            }
//        }
//    }
//
//    static func updateMembershipCard(code:String, typeName:String, typeLevel:Int, balance:Int, point:Int) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.code = code
//            info?.typeName = typeName
//            info?.typeLevel = typeLevel
//            info?.balance = balance
//            info?.point = point
//        }
//    }
//
//    static func updateAccessToken(accessToken:String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.accessToken = accessToken
//        }
//    }
//
//    static func updateIsConnect(isConnect:Bool) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.isConnect = isConnect
//        }
//    }
//
//    static func updatePhone(phone:String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.phone = phone
//        }
//    }
    
}
